package com.dike.modules.domain.device.common.dto;

import com.dike.modules.domain.enums.device.BrandCode;
import com.dike.modules.domain.enums.device.DeviceCategoryCode;
import lombok.*;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Class       : RegisterDeviceDto
 * Author      : 조 준 희
 * Description : Class Description
 * History     : [2022-09-23] - 조 준희 - Class Create
 */

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegisterDeviceDto {

    private DeviceCategoryCode deviceType;
    private BrandCode brandCode;

    @NotNull(message = "series is null")
    private String series;
    private Integer buyYear;

    @NotBlank(message = "reviewKeywords is null")
    private String reviewKeywords;

    @NotNull(message = "grade is null")
    private Integer grade;

    private String review;
}

