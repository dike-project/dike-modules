package com.dike.modules.domain.device;

import com.dike.modules.domain.device.watch.Watch;
import com.dike.modules.domain.enums.device.BrandCode;
import com.dike.modules.domain.enums.device.DeviceCategoryCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReviewKeywordsRepository extends JpaRepository<ReviewKeywords, Long> {

    List<ReviewKeywords> findAllByDeviceCategoryCodeOrderByOrderIdxAsc(DeviceCategoryCode deviceCategoryCode);
}
