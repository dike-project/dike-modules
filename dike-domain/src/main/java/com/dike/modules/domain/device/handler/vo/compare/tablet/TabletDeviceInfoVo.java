package com.dike.modules.domain.device.handler.vo.compare.tablet;


import com.dike.modules.domain.device.handler.vo.compare.DeviceInfoVo;
import com.dike.modules.domain.enums.DikeEnumDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.querydsl.core.Tuple;
import com.querydsl.core.types.MappingProjection;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import static com.dike.modules.domain.device.handler.vo.compare.DeviceInfoVo.toList;
import static com.dike.modules.domain.device.tablet.QTablet.tablet;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TabletDeviceInfoVo implements DeviceInfoVo {


    public static final TabletDeviceInfoVoProjection TABLET_DEVICE_INFO_VO_PROJECTION = new TabletDeviceInfoVoProjection();
    private Long tabletId;
    private DikeEnumDto brandCode;
    private String modelSeriesName;
    private String imagePath;
    @JsonProperty("등록년월")
    private Date registeredAt;
    @JsonProperty("화면크기")
    private Double screenSizeInch;
    @JsonProperty("무게")
    private Double weightG;
    @JsonProperty("해상도")
    private String screenResolution;
    @JsonProperty("램")
    private Double ramSizeGB;
    @JsonProperty("저장용량")
    private Double storageCapacityGB;
    @JsonProperty("충전단자")
    private DikeEnumDto chargingType;
    @JsonProperty("베터리")
    private Double batteryTimeHour;
    @JsonProperty("통신타입")
    private DikeEnumDto networkType;
    @JsonProperty("색상")
    private List<String> colors;
    @JsonProperty("출고가")
    private Integer originalPrice;
    @JsonProperty("펜슬지원")
    private String pencilSupport;

    public static class TabletDeviceInfoVoProjection extends MappingProjection<TabletDeviceInfoVo> {

        public TabletDeviceInfoVoProjection() {
            super(TabletDeviceInfoVo.class,
                    tablet.tabletId,
                    tablet.brandCode,
                    tablet.series,
                    tablet.imagePath,
                    tablet.registeredAt,
                    tablet.screenInch,
                    tablet.screenResolution,
                    tablet.weightG,
                    tablet.ramGB,
                    tablet.storageCapacityGB,
                    tablet.chargingType,
                    tablet.batterTimeHour,
                    tablet.networkType,
                    tablet.colors,
                    tablet.originalPrice
            );
        }

        @Override
        protected TabletDeviceInfoVo map(Tuple row) {
            return TabletDeviceInfoVo.builder()
                    .tabletId(row.get(tablet.tabletId))
                    .brandCode(Optional.ofNullable(row.get(tablet.brandCode)).map(DikeEnumDto::from).orElse(null))
                    .modelSeriesName(row.get(tablet.series))
                    .imagePath(row.get(tablet.imagePath))
                    .registeredAt(row.get(tablet.registeredAt))
                    .screenSizeInch(row.get(tablet.screenInch))
                    .screenResolution(row.get(tablet.screenResolution))
                    .weightG(row.get(tablet.weightG))
                    .ramSizeGB(row.get(tablet.ramGB))
                    .storageCapacityGB(row.get(tablet.storageCapacityGB))
                    .chargingType(Optional.ofNullable(row.get(tablet.chargingType)).map(DikeEnumDto::from).orElse(null))
                    .batteryTimeHour(row.get(tablet.batterTimeHour))
                    .networkType(Optional.ofNullable(row.get(tablet.networkType)).map(DikeEnumDto::from).orElse(null))
                    .colors(toList(row.get(tablet.colors)))
                    .originalPrice(row.get(tablet.originalPrice))
                    .build();
        }
    }
}
