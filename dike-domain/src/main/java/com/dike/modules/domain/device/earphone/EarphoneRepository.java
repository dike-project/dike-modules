package com.dike.modules.domain.device.earphone;

import com.dike.modules.domain.device.laptop.Laptop;
import com.dike.modules.domain.enums.device.BrandCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EarphoneRepository extends JpaRepository<Earphone, Long> {
    boolean existsByEarphoneIdAndBrandCode(Long earphoneId, String brandCode);

    @Query(value = "SELECT m FROM Earphone m WHERE m.brandCode = :#{#brandCode} AND REPLACE(m.modelName,' ','') LIKE CONCAT('%', :modelName,'%')")
    List<Earphone> findAllByBrandCodeAndModelNameContaining(BrandCode brandCode, String modelName);
}
