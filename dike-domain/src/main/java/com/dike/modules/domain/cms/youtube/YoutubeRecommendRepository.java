package com.dike.modules.domain.cms.youtube;

import com.dike.modules.domain.cms.youtube.entity.YoutubeRecommend;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface YoutubeRecommendRepository  extends JpaRepository<YoutubeRecommend, String> {

    List<YoutubeRecommend> findAllByOrderByOrderIdxAsc();

}
