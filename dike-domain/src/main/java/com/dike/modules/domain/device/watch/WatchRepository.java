package com.dike.modules.domain.device.watch;

import com.dike.modules.domain.enums.device.BrandCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WatchRepository extends JpaRepository<Watch, Long>, WatchRepositoryCustom {

    boolean existsByWatchIdAndBrandCode(Long watchId, String brandCode);

    boolean existsByBrandCodeAndSeries(BrandCode brandCode, String series);

    @Query(value = "SELECT m FROM Watch m WHERE m.brandCode = :#{#brandCode} AND " +
            "REPLACE(m.modelName,' ','') LIKE CONCAT('%', :modelName,'%')")
    List<Watch> findAllByBrandCodeAndModelNameContaining(BrandCode brandCode, String modelName);

    @Query(value = "SELECT DISTINCT m.series FROM Watch m WHERE m.brandCode = :#{#brandCode} AND " +
            "REPLACE(m.series,' ','') LIKE CONCAT('%', :searchWord,'%') ORDER BY m.series DESC")
    List<String> findModelSeriesByBrandCodeAndSearchWord(BrandCode brandCode, String searchWord);

}
