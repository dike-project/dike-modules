package com.dike.modules.domain.device.watch;

import com.dike.modules.domain.device.handler.condition.WatchDeviceInfoCondition;
import com.dike.modules.domain.device.handler.vo.compare.watch.WatchAttributeDataVo;
import com.dike.modules.domain.device.handler.vo.compare.watch.WatchDeviceInfoVo;
import com.dike.modules.domain.enums.device.BrandCode;

import java.util.List;

public interface WatchRepositoryCustom {

    List<WatchAttributeDataVo> getWatchAttributeDataVos(BrandCode brandCode, String modelSeriesName);

    WatchDeviceInfoVo getWatchDeviceInfoVo(WatchDeviceInfoCondition condition);
}
