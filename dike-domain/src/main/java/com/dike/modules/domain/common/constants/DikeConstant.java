package com.dike.modules.domain.common.constants;

public final class DikeConstant {

    public static final String PASSWORD_RECOVER_TOKEN = "PASSWORD-RECOVER-TOKEN";
}
