package com.dike.modules.domain.cms.policy;

import com.dike.modules.domain.cms.BrandDevice.DeviceCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PolicyRepository extends JpaRepository<Policy, Long> {

    List<Policy> findAll();
}
