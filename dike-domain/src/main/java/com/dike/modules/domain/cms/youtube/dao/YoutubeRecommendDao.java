package com.dike.modules.domain.cms.youtube.dao;

import com.querydsl.core.annotations.QueryProjection;

public class YoutubeRecommendDao {
    private String categoryCode;
    private String categoryName;
    private String videoId;

    @QueryProjection
    public YoutubeRecommendDao(String categoryCode, String categoryName, String videoId) {
        this.categoryCode = categoryCode;
        this.categoryName = categoryName;
        this.videoId = videoId;
    }
}
