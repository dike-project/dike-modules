package com.dike.modules.domain.device;

import com.dike.modules.domain.common.response.ApiStatusCode;
import com.dike.modules.domain.device.common.dto.ReviewKeywordDto;
import com.dike.modules.domain.device.common.dto.SearchDeviceResponseDto;
import com.dike.modules.domain.device.common.dto.SearchKeywordResultDto;
import com.dike.modules.domain.device.common.dto.compare.CompareDeviceListDto;
import com.dike.modules.domain.device.handler.DeviceCategoryHandler;
import com.dike.modules.domain.device.handler.vo.compare.CompareDeviceResultVo;
import com.dike.modules.domain.device.handler.vo.compare.DeviceCompareAttributeResponse;
import com.dike.modules.domain.enums.device.BrandCode;
import com.dike.modules.domain.enums.device.DeviceCategoryCode;
import com.dike.modules.domain.exception.DikeServiceException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;


/**
 * Class       : DeviceService Author      : 조 준 희 Description : Class Description History     :
 * [2022-09-29] - 조 준희 - Class Create
 */
@Service
@RequiredArgsConstructor
public class DeviceService {

    private final DikeUserDeviceRepository dikeUserDeviceRepo;
    private final ReviewKeywordsRepository reviewKeywordsRepository;
    private final List<DeviceCategoryHandler> deviceCategoryHandlers;
    private Map<DeviceCategoryCode, DeviceCategoryHandler> deviceHandlerMap;

    @PostConstruct
    public void init() {

        deviceHandlerMap = new HashMap<>();
        Arrays.stream(DeviceCategoryCode.values()).forEach(deviceCategoryCode -> {
            deviceCategoryHandlers.stream()
                    .filter(deviceHandler -> deviceHandler.isSupport(deviceCategoryCode)).findFirst()
                    .ifPresent(selectedHandler -> deviceHandlerMap.put(deviceCategoryCode, selectedHandler));
        });
    }

    public List<SearchDeviceResponseDto> findAllByModelName(DeviceCategoryCode deviceCategoryCode,
                                                            BrandCode brandCode, String targetModelName) {

        final DeviceCategoryHandler deviceCategoryHandler = getCurrentDeviceHandler(deviceCategoryCode);

        targetModelName = targetModelName.replace(" ", "");

        return deviceCategoryHandler.searchDeviceBy(brandCode, targetModelName);
    }

    public SearchKeywordResultDto searchDeviceModelSeriesBy(DeviceCategoryCode deviceCategoryCode,
                                                            BrandCode brandCode, String searchWord) {
        final DeviceCategoryHandler deviceCategoryHandler = getCurrentDeviceHandler(deviceCategoryCode);
        final String refinedSearchWord = searchWord.replace(" ", "");

        return SearchKeywordResultDto.builder()
                .keywords(deviceCategoryHandler.searchDeviceModelSeriesBy(brandCode, refinedSearchWord))
                .build();
    }

    private DeviceCategoryHandler getCurrentDeviceHandler(DeviceCategoryCode deviceCategoryCode) {
        return Optional.ofNullable(deviceHandlerMap.get(deviceCategoryCode)).orElseThrow(
                () -> new DikeServiceException(ApiStatusCode.SYSTEM_ERROR, "Not exists device handler."));
    }


    public List<ReviewKeywordDto> findAllByDeviceCategoryKeywords(
            DeviceCategoryCode deviceCategoryCode) {

        List<ReviewKeywordDto> reviewKeywords = reviewKeywordsRepository.findAllByDeviceCategoryCodeOrderByOrderIdxAsc(
                deviceCategoryCode).stream().map(e -> ReviewKeywordDto.builder().id(e.getSeq()).orderIdx(e.getOrderIdx())
                .reviewKeyword(e.getKeyword()).deviceCategoryCode(deviceCategoryCode).build()).collect(Collectors.toList());

        return reviewKeywords;
    }

    // 유저 디바이스 등록
    public void setUserDevice(String userId, BrandCode brandCode,
                              DeviceCategoryCode deviceCategoryCode, String series, String reviewKeywords, int buyYear,
                              int grade, String review) {
        try {
            final DeviceCategoryHandler deviceCategoryHandler = getCurrentDeviceHandler(deviceCategoryCode);


            DikeUserDevice dikeUserDevice = DikeUserDevice.builder().loginId(userId)
                    .deviceCategoryCode(deviceCategoryCode).brandCode(brandCode).series(series)
                    .buyYear(buyYear).reviewKeywords(reviewKeywords).grade(grade).review(review).build();

            dikeUserDeviceRepo.save(dikeUserDevice);
        } catch (Exception e) {
            throw e;
        }
    }

    public DeviceCompareAttributeResponse getDeviceCompareAttributes(DeviceCategoryCode deviceCategoryCode,
                                                                     BrandCode brandCode, String modelSeriesName) {

        final DeviceCategoryHandler deviceCategoryHandler = getCurrentDeviceHandler(deviceCategoryCode);

        if (!deviceCategoryHandler.checkModelSeriesExist(brandCode, modelSeriesName)) {
            throw new DikeServiceException(ApiStatusCode.INVALID_PARAMETER, "존재하지 않는 모델 시리즈명입니다.");
        }

        return deviceCategoryHandler.getDeviceAttributeVo(brandCode, modelSeriesName).from();
    }

    public CompareDeviceResultVo compareDevices(DeviceCategoryCode deviceCategoryCode,
                                                CompareDeviceListDto compareDeviceListDto) {
        final DeviceCategoryHandler deviceCategoryHandler = getCurrentDeviceHandler(deviceCategoryCode);

        return CompareDeviceResultVo.builder()
                .result(deviceCategoryHandler.getDeviceInfoVos(compareDeviceListDto))
                .build();

    }
}
