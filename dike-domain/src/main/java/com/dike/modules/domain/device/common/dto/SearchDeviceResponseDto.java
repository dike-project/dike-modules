package com.dike.modules.domain.device.common.dto;

import com.dike.modules.domain.enums.device.BrandCode;
import com.dike.modules.domain.enums.device.DeviceCategoryCode;
import lombok.*;

/**
 * Class       : SearchDeviceResponseDto
 * Author      : 조 준 희
 * Description : Class Description
 * History     : [2022-09-23] - 조 준희 - Class Create
 */
@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SearchDeviceResponseDto {
    private Long deviceId;
    private BrandCode brandCode;
    private DeviceCategoryCode deviceCategoryCode;
    private String deviceModelName;
}

