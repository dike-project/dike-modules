package com.dike.modules.domain.device.handler.vo.compare.mobile;

import com.querydsl.core.Tuple;
import com.querydsl.core.types.MappingProjection;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import static com.dike.modules.domain.device.mobile.QMobile.mobile;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MobileAttributeDataVo {

    public static MobileAttributeDataVoProjection MOBILE_ATTRIBUTE_DATA_VO_PROJECTION = new MobileAttributeDataVoProjection();
    private Integer storageCapacityGB;

    public static class MobileAttributeDataVoProjection extends MappingProjection<MobileAttributeDataVo> {

        public MobileAttributeDataVoProjection() {
            super(MobileAttributeDataVo.class,
                    mobile.storageCapacityGB);
        }

        @Override
        protected MobileAttributeDataVo map(Tuple row) {
            return MobileAttributeDataVo.builder()
                    .storageCapacityGB(row.get(mobile.storageCapacityGB))
                    .build();
        }
    }
}
