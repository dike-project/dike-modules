package com.dike.modules.domain.device.handler.vo.compare.tablet;

import com.dike.modules.domain.enums.device.NetworkType;
import com.querydsl.core.Tuple;
import com.querydsl.core.types.MappingProjection;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import static com.dike.modules.domain.device.tablet.QTablet.tablet;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TabletAttributeDataVo {

    public static TabletAttributeDataVoProjection TABLET_ATTRIBUTE_DATA_VO_PROJECTION = new TabletAttributeDataVoProjection();
    private Double screenInch;
    private NetworkType networkType;
    private Double storageCapacityGB;

    public static class TabletAttributeDataVoProjection extends MappingProjection<TabletAttributeDataVo> {

        public TabletAttributeDataVoProjection() {
            super(TabletAttributeDataVo.class,
                    tablet.screenInch,
                    tablet.networkType,
                    tablet.storageCapacityGB);
        }

        @Override
        protected TabletAttributeDataVo map(Tuple row) {
            return TabletAttributeDataVo.builder()
                    .screenInch(row.get(tablet.screenInch))
                    .networkType(row.get(tablet.networkType))
                    .storageCapacityGB(row.get(tablet.storageCapacityGB))
                    .build();
        }
    }
}
