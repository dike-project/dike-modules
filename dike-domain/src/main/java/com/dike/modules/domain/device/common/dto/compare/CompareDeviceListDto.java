package com.dike.modules.domain.device.common.dto.compare;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.util.List;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CompareDeviceListDto {

    @NotEmpty(message = "비교할 기기 정보는 필수입니다.")
    private List<CompareDeviceDto> compareDevices;
}
