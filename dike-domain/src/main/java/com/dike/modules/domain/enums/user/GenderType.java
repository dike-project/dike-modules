package com.dike.modules.domain.enums.user;

import com.dike.modules.domain.enums.DikeEnum;

public enum GenderType implements DikeEnum {
    FEMALE("여자"),
    MALE("남자"),
    NONE("-");

    private final String description;

    GenderType(String description) {
        this.description = description;
    }

    @Override
    public String getName() {
        return this.name();
    }

    @Override
    public String getDescription() {
        return this.description;
    }

    public static GenderType of(String name) {
        try {
            return GenderType.valueOf(name);
        } catch(IllegalArgumentException e) {
            throw new IllegalArgumentException(String.format("[%s]는 유효한 성별 유형이 아닙니다.", name));
        }
    }


}
