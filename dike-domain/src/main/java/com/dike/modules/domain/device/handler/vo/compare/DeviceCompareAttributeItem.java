package com.dike.modules.domain.device.handler.vo.compare;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DeviceCompareAttributeItem {
    private List<Object> value;
}
