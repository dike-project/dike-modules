package com.dike.modules.domain.enums.device;

import com.dike.modules.domain.enums.DikeEnum;

public enum ChargingType implements DikeEnum {
    C_TYPE("USB타입C"),
    LIGHTENING_8_PIN("라이트닝8핀"),
    MICRO_5_PIN("마이크로5핀"),
    DC_ADAPTER("DC어댑터"),
    MAG_SAFE3("MagSafe 3")
    ;


    private String description;

    ChargingType(String description) {
        this.description = description;
    }
    @Override
    public String getName() {
        return this.name();
    }

    @Override
    public String getDescription() {
        return this.description;
    }
}
