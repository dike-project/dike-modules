package com.dike.modules.domain.cms.BrandDevice;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BrandDeviceMappingRepository extends JpaRepository<BrandDeviceMapping, Long> {
    public List<BrandDeviceMapping> findAllByDeviceCategoryEntity(DeviceCategory deviceCategory);

}
