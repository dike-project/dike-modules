package com.dike.modules.domain.device;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Class       : DikeUserDeviceRepository
 * Author      : 조 준 희
 * Description : Class Description
 * History     : [2022-09-30] - 조 준희 - Class Create
 */
@Repository
public interface DikeUserDeviceRepository extends JpaRepository<DikeUserDevice,Long> {
}
