package com.dike.modules.domain.auth;


import com.dike.modules.domain.auth.dto.*;
import com.dike.modules.domain.auth.token.*;
import com.dike.modules.domain.auth.user.CustomUserDetails;
import com.dike.modules.domain.common.auth.JwtTokenProvider;
import com.dike.modules.domain.common.response.ApiStatusCode;
import com.dike.modules.domain.common.utils.HashUtil;
import com.dike.modules.domain.common.utils.NicknameGenerator;
import com.dike.modules.domain.configuration.redis.CacheKey;
import com.dike.modules.domain.enums.user.UserRole;
import com.dike.modules.domain.exception.DikeServiceException;
import com.dike.modules.domain.user.DikeUser;
import com.dike.modules.domain.user.DikeUserService;
import com.google.common.collect.Lists;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.StringJoiner;

@Service
@RequiredArgsConstructor
public class AuthService {

    private final JwtTokenProvider jwtTokenProvider;
    private final PasswordEncoder passwordEncoder;
    private final DikeUserService dikeUserService;
    private final NicknameGenerator nicknameGenerator;
    private final RefreshTokenRepository refreshTokenRepository;
    private final LogoutTokenRepository logoutTokenRepository;
    private final PasswordRecoverTokenRepository passwordRecoverTokenRepository;

    public void join(UserJoinDto joinDto) {
        joinDto.setPassword(passwordEncoder.encode(joinDto.getPassword()));
        DikeUser user = UserJoinDto.from(joinDto, UserRole.ROLE_USER);
        setNickname(user);
        dikeUserService.addDikeUser(user);
    }

    public boolean existsPhoneNum(String phone){
        return dikeUserService.existsPhoneNumber(phone);
    }

    public void joinAdmin(UserJoinDto joinDto) {
        joinDto.setPassword(passwordEncoder.encode(joinDto.getPassword()));
        dikeUserService.addDikeUser(UserJoinDto.from(joinDto, UserRole.ROLE_ADMIN));
    }

    public LoginIdCheckDto checkDuplicationId(String loginId) {
        Boolean isDuplicated = dikeUserService.checkIdDuplicated(loginId);
        return LoginIdCheckDto.builder()
            .isDuplicated(isDuplicated)
            .build();
    }

    public UserTokenDto login(UserLoginDto loginDto) throws DikeServiceException {
        DikeUser user = dikeUserService.findByLoginId(loginDto.getLoginId());
        checkPassword(loginDto.getPassword(), user.getPassword());

        Authentication authentication =new UsernamePasswordAuthenticationToken(
                new CustomUserDetails(user.getLoginId(), "",null) {
                }, null);
        UserTokenDto aToken = jwtTokenProvider.createTokenFromAuthentication( authentication);

        return aToken;
    }

    @CacheEvict(value = CacheKey.USER, key = "#loginId")
    public void logout(UserTokenDto tokenDto, String loginId) {
        String accessToken = resolveToken(tokenDto.getAccessToken());
        long remainedExpireTime = jwtTokenProvider.getRemainedExpireTime(accessToken);
        refreshTokenRepository.deleteById(loginId);
        logoutTokenRepository.save(LogoutToken.of(accessToken, loginId, remainedExpireTime));
    }

    public UserTokenDto reissue(String refreshToken) throws DikeServiceException {
        refreshToken = resolveToken(refreshToken);
        String currentLoginId = getLoginIdFromSecurityContext().getLoginId();

        RefreshToken cachedRefreshToken = refreshTokenRepository.findById(currentLoginId)
            .orElseThrow(() -> new DikeServiceException(ApiStatusCode.NOT_EXIST_REFRESH_TOKEN));

        if (cachedRefreshToken.getRefreshToken().equals(refreshToken)) {
            UserTokenDto aToken = jwtTokenProvider.createTokenFromAuthentication(
                    new UsernamePasswordAuthenticationToken(new CustomUserDetails(currentLoginId,"", null),null));

            return aToken;
        }
        throw new DikeServiceException(ApiStatusCode.NOT_MATCHED_REFRESH_TOKEN);
    }

    public boolean checkValidLoginIdWithPhoneNumber(String loginId, String phoneNumber) {
        return dikeUserService.checkValidLoginIdWithPhoneNumber(loginId, phoneNumber);
    }

    public String createPasswordRecoverToken(String loginId) {

        StringJoiner joiner = new StringJoiner(",");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date currentTime = new Date();
        String passwordToken = HashUtil.sha256(joiner.add(loginId).add(simpleDateFormat.format(currentTime)).toString());
        passwordRecoverTokenRepository.save(PasswordRecoverToken.of(loginId, passwordToken, currentTime.getTime()));

        return passwordToken;
    }

    public void recoverPassword(String token, PasswordRecoverDto dto) {
        PasswordRecoverToken passwordRecoverToken = passwordRecoverTokenRepository.findById(token)
            .orElseThrow(() -> new DikeServiceException(ApiStatusCode.TOKEN_NOT_EXIST));

        String loginId = passwordRecoverToken.getLoginId();
        String encodedNewPassword = passwordEncoder.encode(dto.getNewPassword());
        dikeUserService.recoverPassword(loginId, encodedNewPassword);
        passwordRecoverTokenRepository.deleteById(token);
    }

    private String resolveToken(String token) {
        return token.substring(7);
    }

    private void checkPassword(String input, String source) throws DikeServiceException {
        if (!passwordEncoder.matches(input, source)) {
            throw new DikeServiceException(ApiStatusCode.UNAUTHORIZED);
        }
    }

    private CustomUserDetails getLoginIdFromSecurityContext() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomUserDetails userDetails = (CustomUserDetails) authentication.getPrincipal();
        return userDetails;
    }

    private void setNickname(DikeUser dikeUser) {
        boolean isDuplicated = true;
        List<String> candidates = Lists.newArrayList();

        while (isDuplicated) {
            candidates = nicknameGenerator.generatorNickName();
            isDuplicated = dikeUserService.checkNicknameDuplicated(candidates);
        }

        Random random = new Random();
        int index = random.ints(0, 2).findAny().getAsInt();

        if (!CollectionUtils.isEmpty(candidates)) {
            dikeUser.setNickname(candidates.get(index));
        }
    }

    public void delete(String loginId) {
        dikeUserService.deleteDikeUser(loginId);
    }
}
