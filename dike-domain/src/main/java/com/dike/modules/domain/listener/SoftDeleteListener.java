package com.dike.modules.domain.listener;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.util.Date;
import java.util.Objects;

public class SoftDeleteListener {

    @PreUpdate
    @PrePersist
    public void setDeletedAt(final SoftDelete entity) {
        if(Objects.nonNull(entity.getDeleted()) && entity.getDeleted()) {
            entity.setDeletedAt(new Date());
        }
    }
}
