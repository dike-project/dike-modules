package com.dike.modules.domain.device.tablet;

import com.dike.modules.domain.device.handler.condition.TabletDeviceInfoCondition;
import com.dike.modules.domain.device.handler.vo.compare.tablet.TabletAttributeDataVo;
import com.dike.modules.domain.device.handler.vo.compare.tablet.TabletDeviceInfoVo;
import com.dike.modules.domain.enums.device.BrandCode;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.StringTemplate;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;

import java.util.List;
import java.util.Objects;

import static com.dike.modules.domain.device.handler.vo.compare.tablet.TabletAttributeDataVo.TABLET_ATTRIBUTE_DATA_VO_PROJECTION;
import static com.dike.modules.domain.device.handler.vo.compare.tablet.TabletDeviceInfoVo.TABLET_DEVICE_INFO_VO_PROJECTION;

public class TabletRepositoryCustomImpl extends QuerydslRepositorySupport implements TabletRepositoryCustom {

    private final QTablet qTablet = QTablet.tablet;


    public TabletRepositoryCustomImpl() {
        super(Tablet.class);
    }


    @Override
    public List<String> findModelSeriesByBrandCodeAndSearchWord(BrandCode brandCode, String searchWord) {
        StringTemplate series = Expressions.stringTemplate("replace({0},'  ','')", qTablet.series);
        return from(qTablet)
                .where(qTablet.brandCode.eq(brandCode),
                        series.contains(searchWord))
                .select(qTablet.series)
                .distinct()
                .orderBy(qTablet.series.desc())
                .fetch();
    }

    @Override
    public List<TabletAttributeDataVo> getTabletAttributeDataVos(BrandCode brandCode, String modelSeriesName) {
        return from(qTablet)
                .where(qTablet.brandCode.eq(brandCode),
                        qTablet.series.eq(modelSeriesName))
                .select(TABLET_ATTRIBUTE_DATA_VO_PROJECTION)
                .distinct()
                .fetch();
    }

    @Override
    public TabletDeviceInfoVo getTabletDeviceInfoVo(TabletDeviceInfoCondition condition) {
        return from(qTablet)
                .where(getTabletDeviceInfoConditions(condition))
                .select(TABLET_DEVICE_INFO_VO_PROJECTION)
                .fetchFirst();
    }

    private BooleanBuilder getTabletDeviceInfoConditions(TabletDeviceInfoCondition condition) {
        BooleanBuilder predicates = new BooleanBuilder();

        predicates.and(qTablet.brandCode.eq(condition.getBrandCode()));
        predicates.and(qTablet.series.eq(condition.getModelSeriesName()));

        if (Objects.nonNull(condition.getScreenSize())) {
            predicates.and(qTablet.screenInch.eq(condition.getScreenSize()));
        }

        if (Objects.nonNull(condition.getStorageCapacity())) {
            predicates.and(qTablet.storageCapacityGB.eq(condition.getStorageCapacity()));
        }

        if (Objects.nonNull(condition.getNetworkType())) {
            predicates.and(qTablet.networkType.eq(condition.getNetworkType()));
        }

        return predicates;
    }

}
