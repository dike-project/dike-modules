package com.dike.modules.domain.cms.BrandDevice;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DeviceCategoryRepository extends JpaRepository<DeviceCategory, Long> {


}
