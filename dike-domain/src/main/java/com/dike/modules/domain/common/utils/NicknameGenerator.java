package com.dike.modules.domain.common.utils;

import com.dike.modules.domain.common.utils.dto.NicknameResultDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import javax.annotation.PostConstruct;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON;


@Slf4j
@Component
public class NicknameGenerator {


    @Value("${nickname.api-url:}")
    private String apiUrl;

    private WebClient webClient;

    @PostConstruct
    public void init() {
        webClient = WebClient.builder()
            .baseUrl(apiUrl)
            .build();
    }

    public List<String> generatorNickName() {

        NicknameResultDto result = webClient.get()
            .uri(uriBuilder -> uriBuilder.path("/").query("format=json&count=2").build())
            .accept(APPLICATION_JSON)
            .retrieve()
            .bodyToMono(NicknameResultDto.class)
            .block();
        return result.getWords();
    }
}
