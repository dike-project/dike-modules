package com.dike.modules.domain.auth.certification;

import org.springframework.data.repository.CrudRepository;

public interface CertificationNumberRepository extends CrudRepository<CertificationNumber, String> {
}
