package com.dike.modules.domain.exception;

import com.dike.modules.domain.common.response.ApiStatusCode;
import com.dike.modules.domain.common.response.ResponseJsonObject;

/**
 * Description : 디케 서비스 사용자 정의 Exception
 * Author      : 조 준 희
 * History     : [2023-06-01] - 조 준 희 - Create
 */
public class DikeServiceException extends RuntimeException {
    private final ApiStatusCode errorStatusCode;
    private final ResponseJsonObject responseJsonObject;

    public ResponseJsonObject getResponseJsonObject() {
        return responseJsonObject;
    }

    public DikeServiceException(ApiStatusCode apiStatusCode) {
        this.errorStatusCode = apiStatusCode;
        responseJsonObject = ResponseJsonObject.builder().code(errorStatusCode).build();
    }

    public DikeServiceException(ApiStatusCode apiStatusCode, String msg) {
        super(msg);
        this.errorStatusCode = apiStatusCode;
        responseJsonObject = ResponseJsonObject.builder().code(errorStatusCode).msg(msg).build();
    }
}

