package com.dike.modules.domain.device.handler.condition;

import com.dike.modules.domain.device.common.dto.compare.CompareDeviceDto;
import com.dike.modules.domain.device.common.dto.compare.MobileCompareAttributeDto;
import com.dike.modules.domain.enums.device.BrandCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MobileDeviceInfoCondition {

    private BrandCode brandCode;
    private String modelSeriesName;
    private Integer storageCapacity;

    public static MobileDeviceInfoCondition from(CompareDeviceDto dto) {
        MobileCompareAttributeDto attributeDto = (MobileCompareAttributeDto) dto.getCompareAttributes();
        return MobileDeviceInfoCondition.builder()
                .brandCode(dto.getBrandCode())
                .modelSeriesName(dto.getModelSeriesName())
                .storageCapacity(attributeDto.getStorageCapacity())
                .build();
    }
}
