package com.dike.modules.domain.common.utils.dto;


import lombok.*;

import java.util.List;

@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class NicknameResultDto {

    private List<String> words;
    private String seed;

}
