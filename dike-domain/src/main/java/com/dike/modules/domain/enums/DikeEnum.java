package com.dike.modules.domain.enums;

public interface DikeEnum {
    String getName();
    String getDescription();
}
