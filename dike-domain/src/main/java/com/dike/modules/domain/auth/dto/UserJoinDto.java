package com.dike.modules.domain.auth.dto;


import com.dike.modules.domain.enums.user.GenderType;
import com.dike.modules.domain.enums.user.UserRole;
import com.dike.modules.domain.user.DikeUser;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserJoinDto {


    @NotBlank(message = "사용자 아이디는 필수입니다.")
    private String loginId;

    @NotBlank(message = "비밀번호를 입력해주세요.")
    private String password;

    @NotBlank(message = "휴대폰 번호를 입력해주세요.")
    private String phoneNumber;

    @NotNull(message = "개인정보 이용 동의 여부를 체크해주세요.")
    private PolicyDto privacyAllowed;

    @Builder.Default
    private Integer age = 0;

    @Builder.Default
    private String gender = "NONE";

    public static DikeUser from(UserJoinDto dto, UserRole role) {
        return DikeUser.builder()
            .loginId(dto.getLoginId())
            .password(dto.getPassword())
            .phoneNumber(dto.getPhoneNumber())
            .age(dto.getAge())
            .role(role)
            .servicePolicy(dto.getPrivacyAllowed().getServicePolicy()).privacyPolicy(dto.getPrivacyAllowed().getPrivacyPolicy())
                .marketingPolicy(dto.getPrivacyAllowed().getMarketingPolicy())
            .gender(GenderType.of(dto.getGender()))
            .build();
    }

}
