package com.dike.modules.domain.device.tablet;

import com.dike.modules.domain.enums.device.BrandCode;
import com.dike.modules.domain.enums.device.ChargingType;
import com.dike.modules.domain.enums.device.NetworkType;
import com.dike.modules.domain.listener.CreatedAtListener;
import com.dike.modules.domain.listener.SystemMetaData;
import com.dike.modules.domain.listener.UpdatedAtListener;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Data
@Builder
@Entity
@Table(name = "tablet", schema = "public")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@EntityListeners({CreatedAtListener.class, UpdatedAtListener.class})
public class Tablet implements SystemMetaData {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tablet_seq")
    @SequenceGenerator(name = "tablet_seq", sequenceName = "tablet_seq", allocationSize = 1)
    @Column(name = "tablet_id")
    private Long tabletId;

    @Enumerated(EnumType.STRING)
    @Column(name = "brand_code")
    private BrandCode brandCode;

    @Column(name = "series")
    private String series;

    @Column(name = "registered_at")
    @Temporal(TemporalType.DATE)
    private Date registeredAt;

    @Column(name = "model_code")
    private String modelCode;

    @Column(name = "screen_inch")
    private Double screenInch;

    @Column(name = "screen_resolution")
    private String screenResolution;

    @Column(name = "rear_camera")
    private String rearCamera;

    @Column(name = "front_camera")
    private String frontCamera;

    @Enumerated(EnumType.STRING)
    @Column(name = "charging_type")
    private ChargingType chargingType;

    @Column(name = "weight_g")
    private Double weightG;

    @Column(name = "thickness_mm")
    private Double thicknessMM;

    @Column(name = "screen_width_mm")
    private Double screenWidthMM;

    @Column(name = "screen_height_mm")
    private Double screenHeightMM;

    @Enumerated(EnumType.STRING)
    @Column(name = "network_type")
    private NetworkType networkType;

    @Column(name = "ram_gb")
    private Double ramGB;

    @Column(name = "storage_capacity_gb")
    private Double storageCapacityGB;

    @Column(name = "model_name")
    private String modelName;

    @Column(name = "original_price")
    private Integer originalPrice;

    @Column(name = "colors")
    private String colors;

    @Column(name = "pencil_support")
    private Boolean pencilSupport;

    @Column(name = "image_path")
    private String imagePath;

    @Column(name = "battery_time_hour")
    private Double batterTimeHour;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at")
    private Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_at")
    private Date updatedAt;
}
