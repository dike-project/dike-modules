package com.dike.modules.domain.cms.BrandDevice;

import com.dike.modules.domain.enums.device.BrandCode;
import com.dike.modules.domain.listener.CreatedAtListener;
import com.dike.modules.domain.listener.UpdatedAtListener;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * Class       : Brand
 * Author      : 조 준 희
 * Description : Class Description
 * History     : [2022-07-22] - 조 준희 - Class Create
 */
@Entity
@Table(name = "brand", schema = "public")
@EntityListeners({CreatedAtListener.class, UpdatedAtListener.class})
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Brand {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "brand_seq")
    @SequenceGenerator(name = "brand_seq", sequenceName = "brand_seq", allocationSize = 1)
    @Column(name = "brand_id")
    private Long brandId;

    @Enumerated(EnumType.STRING)
    @Column(name = "brand_code")
    private BrandCode brandCode;

    @Column(name = "brand_name")
    private String brandName;

    @Column(name = "brand_name_eng")
    private String brandNameEng;

    @Column(name = "brand_icon")
    private String brandIcon;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at")
    private Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_at")
    private Date updatedAt;

    public BrandResponse toResponse(Brand entity){
        return BrandResponse.builder().brandId(entity.getBrandId())
                .brandCode(entity.getBrandCode())
                .brandName(entity.getBrandName())
                .brandNameEng(entity.getBrandNameEng())
                .brandIcon(entity.getBrandIcon())
                .build();
    }

}
