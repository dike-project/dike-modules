package com.dike.modules.domain.enums.device;


import org.springframework.core.convert.converter.Converter;

/**
 * Class       : BrandCodeRequestConverter
 * Author      : 조 준 희
 * Description : Class Description
 * History     : [2022-09-30] - 조 준희 - Class Create
 */
public class BrandCodeRequestConverter implements Converter<String, BrandCode> {
    @Override
    public BrandCode convert(String source) {

        return BrandCode.valueOf(source);
    }
}
