package com.dike.modules.domain.cms.BrandDevice;


import com.dike.modules.domain.listener.CreatedAtListener;
import com.dike.modules.domain.listener.UpdatedAtListener;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

@EqualsAndHashCode
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners({CreatedAtListener.class, UpdatedAtListener.class})
public class BrandDeviceMappingKeys implements Serializable {
    private Long brandEntity;
    private Long deviceCategoryEntity;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at")
    private Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_at")
    private Date updatedAt;
}
