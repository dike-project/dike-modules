package com.dike.modules.domain.device.handler.vo.compare.watch;

import com.dike.modules.domain.enums.device.NetworkType;
import com.querydsl.core.Tuple;
import com.querydsl.core.types.MappingProjection;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import static com.dike.modules.domain.device.watch.QWatch.watch;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WatchAttributeDataVo {

    public static WatchAttributeDataVoProjection WATCH_ATTRIBUTE_DATA_VO_PROJECTION = new WatchAttributeDataVoProjection();
    private Double screenMM;
    private String materialType;
    private NetworkType networkType;

    public static class WatchAttributeDataVoProjection extends MappingProjection<WatchAttributeDataVo> {

        public WatchAttributeDataVoProjection() {
            super(WatchAttributeDataVo.class,
                    watch.screenMM,
                    watch.materialType,
                    watch.networkType);
        }

        @Override
        protected WatchAttributeDataVo map(Tuple row) {
            return WatchAttributeDataVo.builder()
                    .screenMM(row.get(watch.screenMM))
                    .materialType(row.get(watch.materialType))
                    .networkType(row.get(watch.networkType))
                    .build();
        }
    }
}
