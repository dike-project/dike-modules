package com.dike.modules.domain.device.earphone;


import com.dike.modules.domain.enums.device.BrandCode;
import com.dike.modules.domain.enums.device.ChargingType;
import com.dike.modules.domain.listener.CreatedAtListener;
import com.dike.modules.domain.listener.SystemMetaData;
import com.dike.modules.domain.listener.UpdatedAtListener;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Data
@Builder
@Entity
@Table(name = "earphone", schema = "public")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@EntityListeners({CreatedAtListener.class, UpdatedAtListener.class})
public class Earphone implements SystemMetaData {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "earphone_seq")
    @SequenceGenerator(name = "earphone_seq", sequenceName = "earphone_seq", allocationSize = 1)
    @Column(name = "earphone_id")
    private Long earphoneId;

    @Enumerated(EnumType.STRING)
    @Column(name = "brand_code")
    private BrandCode brandCode;

    @Column(name = "registered_at")
    @Temporal(TemporalType.DATE)
    private Date registeredAt;

    @Column(name = "purpose")
    private String purpose;

    @Column(name = "unit_type")
    private String unitType;

    @Column(name = "active_noise_cancelling")
    private Boolean activeNoiseCancelling;

    @Column(name = "surrounding_listening")
    private Boolean surroundingListening;

    @Column(name = "touch_button")
    private Boolean touchButton;

    @Column(name = "hand_free_call")
    private Boolean handFreeCall;

    @Column(name = "water_proof")
    private String waterProof;

    @Column(name = "play_time_hour")
    private Double playTimeHour;

    @Column(name = "call_time_hour")
    private Double callTimeHour;

    @Column(name = "case_charging_number")
    private Double caseChargingNumber;

    @Enumerated(EnumType.STRING)
    @Column(name = "charging_type")
    private ChargingType chargingType;

    @Column(name = "weight_g")
    private Double weightG;

    @Column(name = "model_code")
    private String modelCode;

    @Column(name = "model_name")
    private String modelName;

    @Column(name = "image_path")
    private String imagePath;

    @Column(name = "original_price")
    private Integer originalPrice;

    @Column(name = "colors")
    private String colors;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at")
    private Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_at")
    private Date updatedAt;
}
