package com.dike.modules.domain.device.handler.vo.compare.watch;


import com.dike.modules.domain.device.handler.vo.compare.DeviceInfoVo;
import com.dike.modules.domain.enums.DikeEnumDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.querydsl.core.Tuple;
import com.querydsl.core.types.MappingProjection;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import static com.dike.modules.domain.device.handler.vo.compare.DeviceInfoVo.toList;
import static com.dike.modules.domain.device.watch.QWatch.watch;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WatchDeviceInfoVo implements DeviceInfoVo {


    public static final WatchDeviceInfoVoProjection WATCH_DEVICE_INFO_VO_PROJECTION = new WatchDeviceInfoVoProjection();
    private Long watchId;
    private DikeEnumDto brandCode;
    private String modelSeriesName;
    private String imagePath;
    @JsonProperty("등록년월")
    private Date registeredAt;
    @JsonProperty("모양")
    private String deviceShape;
    @JsonProperty("화면크기")
    private Double screenSizeMM;
    @JsonProperty("램")
    private Double ramSizeGB;
    @JsonProperty("색상")
    private List<String> colors;
    @JsonProperty("소재")
    private String materialType;
    @JsonProperty("출고가")
    private Integer originalPrice;
    @JsonProperty("운동기능")
    private List<String> exerciseSupport;
    @JsonProperty("건강기능")
    private List<String> healthSupport;
    @JsonProperty("통신타입")
    private DikeEnumDto networkType;
    @JsonProperty("알림기능")
    private List<String> alarmSupport;
    @JsonProperty("AI음성인식")
    private Boolean aiVoiceRecognition;
    @JsonProperty("분실방지")
    private Boolean lossPrevention;
    @JsonProperty("무게")
    private Double weightG;
    @JsonProperty("GPS")
    private Boolean gps;
    @JsonProperty("NFC")
    private Boolean nfc;
    @JsonProperty("무선충전")
    private Boolean wirelessCharging;
    @JsonProperty("방수방진")
    private String waterDustProof;
    @JsonProperty("베터리")
    private Integer batteryTimeHour;
    @JsonProperty("해상도")
    private String screenResolution;
    @JsonProperty("통화기능")
    private Boolean callable;

    public static class WatchDeviceInfoVoProjection extends MappingProjection<WatchDeviceInfoVo> {

        public WatchDeviceInfoVoProjection() {
            super(WatchDeviceInfoVo.class,
                    watch.watchId,
                    watch.brandCode,
                    watch.series,
                    watch.registeredAt,
                    watch.deviceShape,
                    watch.screenResolution,
                    watch.screenMM,
                    watch.ramGB,
                    watch.materialType,
                    watch.imagePath,
                    watch.originalPrice,
                    watch.weightG,
                    watch.exerciseSupport,
                    watch.healthSupport,
                    watch.batteryTimeHour,
                    watch.lossPrevention,
                    watch.wirelessCharging,
                    watch.alarmSupport,
                    watch.aiVoiceRecognition,
                    watch.waterDustProof,
                    watch.networkType,
                    watch.gps,
                    watch.nfc,
                    watch.callable,
                    watch.colors);
        }

        @Override
        protected WatchDeviceInfoVo map(Tuple row) {
            return WatchDeviceInfoVo.builder()
                    .watchId(row.get(watch.watchId))
                    .brandCode(Optional.ofNullable(row.get(watch.brandCode)).map(DikeEnumDto::from).orElse(null))
                    .modelSeriesName(row.get(watch.series))
                    .registeredAt(row.get(watch.registeredAt))
                    .deviceShape(row.get(watch.deviceShape))
                    .screenResolution(row.get(watch.screenResolution))
                    .screenSizeMM(row.get(watch.screenMM))
                    .ramSizeGB(row.get(watch.ramGB))
                    .materialType(row.get(watch.materialType))
                    .imagePath(row.get(watch.imagePath))
                    .originalPrice(row.get(watch.originalPrice))
                    .weightG(row.get(watch.weightG))
                    .exerciseSupport(toList(row.get(watch.exerciseSupport)))
                    .healthSupport(toList(row.get(watch.healthSupport)))
                    .batteryTimeHour(row.get(watch.batteryTimeHour))
                    .lossPrevention(row.get(watch.lossPrevention))
                    .wirelessCharging(row.get(watch.wirelessCharging))
                    .alarmSupport(toList(row.get(watch.alarmSupport)))
                    .aiVoiceRecognition(row.get(watch.aiVoiceRecognition))
                    .waterDustProof(row.get(watch.waterDustProof))
                    .networkType(Optional.ofNullable(row.get(watch.networkType)).map(DikeEnumDto::from).orElse(null))
                    .gps(row.get(watch.gps))
                    .nfc(row.get(watch.nfc))
                    .callable(row.get(watch.callable))
                    .colors(toList(row.get(watch.colors)))
                    .build();
        }
    }
}
