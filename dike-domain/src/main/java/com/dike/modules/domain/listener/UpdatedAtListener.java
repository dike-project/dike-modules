package com.dike.modules.domain.listener;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.util.Date;

public class UpdatedAtListener {

    @PreUpdate
    @PrePersist
    public void setUpdatedAt(final SystemMetaData entity) {
        entity.setUpdatedAt(new Date());
    }
}
