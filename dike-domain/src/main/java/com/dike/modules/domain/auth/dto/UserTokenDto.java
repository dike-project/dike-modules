package com.dike.modules.domain.auth.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserTokenDto {

    @Builder.Default
    private String grantType = "Bearer";

    private String accessToken;

    private String refreshToken;

    public static UserTokenDto of(String accessToken, String refreshToken) {
        return UserTokenDto.builder()
            .accessToken(accessToken)
            .refreshToken(refreshToken)
            .build();
    }
}
