package com.dike.modules.domain.common.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

/**
 * Class       : ResponseJsonObject
 * Author      : 조 준 희
 * Description : Class Description
 * History     : [2022-07-04] - 조 준희 - Class Create
 */
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class ResponseJsonObject {

    // statusCode (not null)
    @Builder.Default
    private ApiStatusCode code = ApiStatusCode.NONE;
    @Builder.Default
    private String msg = null;
    private Object data;


    // --------------- JSON 필드 --------------------
    public int getCode(){ return this.code.getCode(); }

    public String getType() {
        return this.code.getType();
    }

    public String getMsg() {
        // 추가 메시지가 있는 경우
        if(msg != null)
            return msg;

        return this.code.getMessage();
    }
    // data
    public Object getData() {return data;}
    // ---------------------------------------------


    public Boolean errorType(){
        return code.getIsErrorType();
    }
}
