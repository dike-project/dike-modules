package com.dike.modules.domain.enums.device;


import org.springframework.core.convert.converter.Converter;

/**
 * Class       : BrandCodeRequestConverter
 * Author      : 조 준 희
 * Description : Class Description
 * History     : [2022-09-30] - 조 준희 - Class Create
 */
public class DeviceCategoryCodeConverter implements Converter<String, DeviceCategoryCode> {
    @Override
    public DeviceCategoryCode convert(String source) {

        return DeviceCategoryCode.valueOf(source);
    }
}
