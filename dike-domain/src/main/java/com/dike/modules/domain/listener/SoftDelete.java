package com.dike.modules.domain.listener;

import java.io.Serializable;
import java.util.Date;

public interface SoftDelete extends Serializable {

    Boolean getDeleted();
    void setDeletedAt(final Date date);

}
