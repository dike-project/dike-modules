package com.dike.modules.domain.auth.certification;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.TimeToLive;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@RedisHash("certificationNumber")
public class CertificationNumber {

    @Id
    private String id/* phoneNumber */;

    private String certificationNumber;

    @TimeToLive
    private Long expiration;

    public static CertificationNumber of(String phoneNumber, String number, long expireTime) {
        return CertificationNumber.builder()
            .id(phoneNumber)
            .certificationNumber(number)
            .expiration((System.currentTimeMillis() + expireTime) / 1000)
            .build();

    }
}
