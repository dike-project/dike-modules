package com.dike.modules.domain.device.watch;

import com.dike.modules.domain.device.handler.condition.WatchDeviceInfoCondition;
import com.dike.modules.domain.device.handler.vo.compare.watch.WatchAttributeDataVo;
import com.dike.modules.domain.device.handler.vo.compare.watch.WatchDeviceInfoVo;
import com.dike.modules.domain.enums.device.BrandCode;
import com.querydsl.core.BooleanBuilder;
import org.apache.logging.log4j.util.Strings;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;

import java.util.List;
import java.util.Objects;

import static com.dike.modules.domain.device.handler.vo.compare.watch.WatchAttributeDataVo.WATCH_ATTRIBUTE_DATA_VO_PROJECTION;
import static com.dike.modules.domain.device.handler.vo.compare.watch.WatchDeviceInfoVo.WATCH_DEVICE_INFO_VO_PROJECTION;

public class WatchRepositoryCustomImpl extends QuerydslRepositorySupport implements WatchRepositoryCustom {

    private final QWatch qWatch = QWatch.watch;

    public WatchRepositoryCustomImpl() {
        super(Watch.class);
    }

    @Override
    public List<WatchAttributeDataVo> getWatchAttributeDataVos(BrandCode brandCode, String modelSeriesName) {
        return from(qWatch)
                .where(qWatch.brandCode.eq(brandCode),
                        qWatch.series.eq(modelSeriesName))
                .select(WATCH_ATTRIBUTE_DATA_VO_PROJECTION)
                .distinct()
                .fetch();
    }

    @Override
    public WatchDeviceInfoVo getWatchDeviceInfoVo(WatchDeviceInfoCondition condition) {
        return from(qWatch)
                .where(getWatchDeviceInfoConditions(condition))
                .select(WATCH_DEVICE_INFO_VO_PROJECTION)
                .fetchFirst();
    }


    private BooleanBuilder getWatchDeviceInfoConditions(WatchDeviceInfoCondition condition) {
        BooleanBuilder predicates = new BooleanBuilder();

        predicates.and(qWatch.brandCode.eq(condition.getBrandCode()));
        predicates.and(qWatch.series.eq(condition.getModelSeriesName()));

        if (Objects.nonNull(condition.getScreenSize())) {
            predicates.and(qWatch.screenMM.eq(condition.getScreenSize()));
        }

        if (Strings.isNotBlank(condition.getMaterialType())) {
            predicates.and(qWatch.materialType.eq(condition.getMaterialType()));
        }

        if (Objects.nonNull(condition.getNetworkType())) {
            predicates.and(qWatch.networkType.eq(condition.getNetworkType()));
        }

        return predicates;
    }

}
