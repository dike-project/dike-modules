package com.dike.modules.domain.common.sms;

import com.dike.modules.domain.common.response.ApiStatusCode;
import com.dike.modules.domain.auth.certification.CertificationNumber;
import com.dike.modules.domain.auth.certification.CertificationNumberRepository;
import com.dike.modules.domain.auth.dto.CertificationDto;
import com.dike.modules.domain.exception.DikeServiceException;
import com.dike.modules.sms.event.SMSEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import java.util.Random;

@Log4j2
@Service
@RequiredArgsConstructor
public class MessageSendService {


    @Value("${dike.sens.message.expireTime}")
    private Long expireTime;

    private final CertificationNumberRepository certificationNumberRepository;
    private final ApplicationEventPublisher smsEventPublisher;

    public void sendMessageBySens(String receiver) {

        //String number = generateCertificationNumber();
        String number = "111111";
        //smsEventPublisher.publishEvent(new SMSEvent(receiver, number));

        if (certificationNumberRepository.existsById(receiver)) {
            certificationNumberRepository.deleteById(receiver);
        }
        certificationNumberRepository.save(CertificationNumber.of(receiver, number, expireTime));

    }

    private String generateCertificationNumber() {
        Random random = new Random();
        Integer randomNumber = random.ints(100000, 1000000)
            .findFirst()
            .getAsInt();
        return randomNumber.toString();
    }

    public void checkCertification(CertificationDto dto) throws DikeServiceException {

        CertificationNumber certificationNumber = certificationNumberRepository.findById(dto.getReceiver())
            .orElseThrow(() -> new DikeServiceException(ApiStatusCode.NOT_EXIST_CERTIFICATION_NUMBER));

        if (!certificationNumber.getCertificationNumber().equals(dto.getTargetNumber())) {
            throw new DikeServiceException(ApiStatusCode.NOT_MATCHED_CERTIFICATION_NUMBER);
        }

        return;
    }
}
