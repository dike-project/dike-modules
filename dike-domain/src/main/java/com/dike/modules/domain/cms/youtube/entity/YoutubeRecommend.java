package com.dike.modules.domain.cms.youtube.entity;

import com.dike.modules.domain.listener.CreatedAtListener;
import com.dike.modules.domain.listener.UpdatedAtListener;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * Class       : YoutubeCategory
 * Author      : 조 준 희
 * Description : Class Description
 * History     : [2023-01-28] - 조 준희 - Class Create
 */
@Entity
@Table(name = "youtube_recommend", schema = "public")
@EntityListeners({CreatedAtListener.class, UpdatedAtListener.class})
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class YoutubeRecommend {

    @Id
    @Column(name = "category_code")
    private String categoryCode;

    @Column(name = "category_name")
    private String categoryName;

    @Column(name = "videos")
    private String videos;

    @Column(name = "order_idx")
    private Long orderIdx;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at")
    private Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_at")
    private Date updatedAt;
}
