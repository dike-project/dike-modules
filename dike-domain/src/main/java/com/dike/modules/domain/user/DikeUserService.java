package com.dike.modules.domain.user;


import com.dike.modules.domain.common.response.ApiStatusCode;
import com.dike.modules.domain.exception.DikeServiceException;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class DikeUserService {

    private final DikeUserRepository dikeUserRepository;

    public void addDikeUser(DikeUser user) {
        dikeUserRepository.save(user);
    }
    public void deleteDikeUser(String loginId) {
        dikeUserRepository.deleteByLoginId(loginId);
    }

    public DikeUser findByLoginId(String loginId) throws DikeServiceException {
        return dikeUserRepository.findByLoginId(loginId)
            .orElseThrow(() -> new DikeServiceException(ApiStatusCode.UNAUTHORIZED));
    }
    public boolean existsPhoneNumber(String phone) throws DikeServiceException {
        return dikeUserRepository.existsByPhoneNumber(phone);
    }
    public Boolean checkIdDuplicated(String targetId) {
        return dikeUserRepository.findByLoginId(targetId).isPresent();
    }

    public Boolean checkNicknameDuplicated(List<String> candidates) {
        return dikeUserRepository.findFirstByNicknameIsIn(candidates).isPresent();
    }

    public void recoverPassword(String loginId, String newPassword) {
        DikeUser dikeUser = findByLoginId(loginId);
        dikeUser.setPassword(newPassword);
        dikeUserRepository.save(dikeUser);
    }

    public Boolean checkValidLoginIdWithPhoneNumber(String loginId, String phoneNumber) {
        return dikeUserRepository.existsDikeUserByLoginIdAndPhoneNumber(loginId, phoneNumber);
    }
}
