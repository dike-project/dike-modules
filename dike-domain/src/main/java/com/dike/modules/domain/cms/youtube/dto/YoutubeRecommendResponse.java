package com.dike.modules.domain.cms.youtube.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
public class YoutubeRecommendResponse {
    private String youtubeCategoryCode;
    private String youtubeCategoryName;
    private List<YouTubeDto> youtubeList;
}
