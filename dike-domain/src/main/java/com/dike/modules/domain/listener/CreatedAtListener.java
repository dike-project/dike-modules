package com.dike.modules.domain.listener;

import javax.persistence.PrePersist;
import java.util.Date;

public class CreatedAtListener {

    @PrePersist
    public void setCreatedAt(final SystemMetaData entity) {
        entity.setCreatedAt(new Date());
    }
}
