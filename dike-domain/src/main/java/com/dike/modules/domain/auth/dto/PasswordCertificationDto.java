package com.dike.modules.domain.auth.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class PasswordCertificationDto extends CertificationDto {

    @NotBlank(message = "아이디는 필수값입니다.")
    private String loginId;
}
