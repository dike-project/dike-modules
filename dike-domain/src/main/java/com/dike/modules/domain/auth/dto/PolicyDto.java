package com.dike.modules.domain.auth.dto;


import lombok.*;

import javax.validation.constraints.NotNull;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PolicyDto {
    @NotNull(message = "개인정보 이용 동의 여부를 체크해주세요.")
    private Boolean servicePolicy;
    @NotNull(message = "개인정보 이용 동의 여부를 체크해주세요.")
    private Boolean privacyPolicy;
    @Builder.Default
    private Boolean marketingPolicy = false;
}
