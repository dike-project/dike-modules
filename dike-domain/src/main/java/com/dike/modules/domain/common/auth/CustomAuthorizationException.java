package com.dike.modules.domain.common.auth;

import com.dike.modules.domain.common.response.ApiStatusCode;
import com.dike.modules.domain.common.response.ResponseJsonObject;

/**
 * Class       : AuthorizationException
 * Author      : 조 준 희
 * Description : Spring Security JWT 인증 과정에서 생기는 ExceptionHandler에서 사용되어지는 객체.
 * History     : [2022-07-07] - 조 준희 - Class Create
 */
public class CustomAuthorizationException  {
    private final ApiStatusCode errorStatusCode = ApiStatusCode.FORBIDDEN;
    private final ResponseJsonObject responseJsonObject;

    public ResponseJsonObject getResponseJsonObject(){
        return responseJsonObject;
    }

    public CustomAuthorizationException() {

        responseJsonObject = ResponseJsonObject.builder().code(errorStatusCode).build();
    }


}
