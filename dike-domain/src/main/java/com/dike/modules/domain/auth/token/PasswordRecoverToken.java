package com.dike.modules.domain.auth.token;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.TimeToLive;


@Getter
@Builder
@RedisHash("passwordRecover")
@AllArgsConstructor
public class PasswordRecoverToken {

    @Id
    private String passwordToken;

    private String loginId;

    @TimeToLive
    private Long expiration; /* 비밀번호 변경 가능 시간 = 60s */

    public static PasswordRecoverToken of(String loginId,
                                          String token,
                                          Long expiration) {

        return PasswordRecoverToken.builder()
            .passwordToken(token)
            .loginId(loginId)
            .expiration((expiration + 60000) / 1000)
            .build();
    }
}
