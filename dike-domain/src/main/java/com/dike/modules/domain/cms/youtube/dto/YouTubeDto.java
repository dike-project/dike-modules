package com.dike.modules.domain.cms.youtube.dto;

import com.querydsl.core.annotations.QueryProjection;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
public class YouTubeDto {

    private String channelId;
    private String channelTitle; // 채널 제목
    private String channelThumbnail; // 채널 제목
    private String videoTitle; // 동영상 제목
    private String thumbnailPath; //동영상 썸네일 경로
    private String videoId; // 동영상 식별 ID
    private String embed;

    public YouTubeDto(String channelId, String channelTitle, String channelThumbnail, String videoTitle, String thumbnailPath, String videoId, String embed) {
        this.channelId = channelId;
        this.channelTitle = channelTitle;
        this.channelThumbnail = channelThumbnail;
        this.videoTitle = videoTitle;
        this.thumbnailPath = thumbnailPath;
        this.videoId = videoId;
        this.embed = embed;
    }
}

