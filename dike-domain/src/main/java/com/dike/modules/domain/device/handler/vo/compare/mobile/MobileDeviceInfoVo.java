package com.dike.modules.domain.device.handler.vo.compare.mobile;


import com.dike.modules.domain.device.handler.vo.compare.DeviceInfoVo;
import com.dike.modules.domain.enums.DikeEnumDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.querydsl.core.Tuple;
import com.querydsl.core.types.MappingProjection;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import static com.dike.modules.domain.device.handler.vo.compare.DeviceInfoVo.toList;
import static com.dike.modules.domain.device.mobile.QMobile.mobile;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MobileDeviceInfoVo implements DeviceInfoVo {


    public static final MobileDeviceInfoVoProjection MOBILE_DEVICE_INFO_VO_PROJECTION = new MobileDeviceInfoVoProjection();
    private Long mobileId;
    private DikeEnumDto brandCode;
    private String modelSeriesName;
    private String imagePath;
    @JsonProperty("등록년월")
    private Date registeredAt;
    @JsonProperty("화면크기")
    private Double screenSizeInch;
    @JsonProperty("무게")
    private Double weightG;
    @JsonProperty("해상도")
    private String screenResolution;
    @JsonProperty("카메라타입")
    private String cameraType;
    @JsonProperty("후면카메라")
    private String rearCamera;
    @JsonProperty("전면카메라")
    private String frontCamera;
    @JsonProperty("램")
    private Integer ramSizeGB;
    @JsonProperty("저장용량")
    private Integer storageCapacityGB;
    @JsonProperty("충전단자")
    private DikeEnumDto chargingType;
    @JsonProperty("통신타입")
    private DikeEnumDto networkType;
    @JsonProperty("방수방진")
    private String waterDustProof;
    @JsonProperty("보안")
    private String security;
    @JsonProperty("색상")
    private List<String> colors;
    @JsonProperty("출고가")
    private Integer originalPrice;


    public static class MobileDeviceInfoVoProjection extends MappingProjection<MobileDeviceInfoVo> {

        public MobileDeviceInfoVoProjection() {
            super(MobileDeviceInfoVo.class,
                    mobile.mobileId,
                    mobile.brandCode,
                    mobile.series,
                    mobile.imagePath,
                    mobile.registeredAt,
                    mobile.screenInch,
                    mobile.weightG,
                    mobile.screenResolution,
                    mobile.cameraType,
                    mobile.rearCamera,
                    mobile.frontCamera,
                    mobile.ramGB,
                    mobile.storageCapacityGB,
                    mobile.chargingType,
                    mobile.networkType,
                    mobile.waterDustProof,
                    mobile.security,
                    mobile.colors,
                    mobile.originalPrice
            );
        }

        @Override
        protected MobileDeviceInfoVo map(Tuple row) {
            return MobileDeviceInfoVo.builder()
                    .mobileId(row.get(mobile.mobileId))
                    .brandCode(Optional.ofNullable(row.get(mobile.brandCode)).map(DikeEnumDto::from).orElse(null))
                    .modelSeriesName(row.get(mobile.series))
                    .imagePath(row.get(mobile.imagePath))
                    .registeredAt(row.get(mobile.registeredAt))
                    .screenSizeInch(row.get(mobile.screenInch))
                    .weightG(row.get(mobile.weightG))
                    .screenResolution(row.get(mobile.screenResolution))
                    .cameraType(row.get(mobile.cameraType))
                    .rearCamera(row.get(mobile.rearCamera))
                    .frontCamera(row.get(mobile.frontCamera))
                    .ramSizeGB(row.get(mobile.ramGB))
                    .storageCapacityGB(row.get(mobile.storageCapacityGB))
                    .chargingType(Optional.ofNullable(row.get(mobile.chargingType)).map(DikeEnumDto::from).orElse(null))
                    .networkType(Optional.ofNullable(row.get(mobile.networkType)).map(DikeEnumDto::from).orElse(null))
                    .waterDustProof(row.get(mobile.waterDustProof))
                    .security(row.get(mobile.security))
                    .colors(toList(row.get(mobile.colors)))
                    .originalPrice(row.get(mobile.originalPrice))
                    .build();
        }

    }
}
