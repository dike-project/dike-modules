package com.dike.modules.domain.device.tablet;

import com.dike.modules.domain.device.handler.condition.TabletDeviceInfoCondition;
import com.dike.modules.domain.device.handler.vo.compare.tablet.TabletAttributeDataVo;
import com.dike.modules.domain.device.handler.vo.compare.tablet.TabletDeviceInfoVo;
import com.dike.modules.domain.enums.device.BrandCode;

import java.util.List;

public interface TabletRepositoryCustom {

    List<String> findModelSeriesByBrandCodeAndSearchWord(BrandCode brandCode, String searchWord);

    List<TabletAttributeDataVo> getTabletAttributeDataVos(BrandCode brandCode, String modelSeriesName);

    TabletDeviceInfoVo getTabletDeviceInfoVo(TabletDeviceInfoCondition condition);
}
