package com.dike.modules.domain.common.utils;


import com.dike.modules.domain.common.response.ApiStatusCode;
import com.dike.modules.domain.exception.DikeServiceException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class HashUtil {

    public static String sha256(String msg) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(msg.getBytes());
            return byteToHexString(md.digest());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        throw new DikeServiceException(ApiStatusCode.SYSTEM_ERROR, "sha256 hash fail");
    }

    public static String byteToHexString(byte[] data) {
        StringBuilder sb = new StringBuilder();
        for (byte b : data) {
            sb.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }
}
