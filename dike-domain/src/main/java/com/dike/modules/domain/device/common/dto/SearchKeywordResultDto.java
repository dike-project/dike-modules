package com.dike.modules.domain.device.common.dto;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SearchKeywordResultDto {

    private List<String> keywords;

    @JsonIgnore
    public boolean isEmptyKeywords() {
        return CollectionUtils.isEmpty(this.keywords);
    }
}
