package com.dike.modules.domain.cms.BrandDevice;


import com.dike.modules.domain.enums.device.BrandCode;
import com.dike.modules.domain.enums.device.DeviceCategoryCode;
import com.dike.modules.domain.listener.CreatedAtListener;
import com.dike.modules.domain.listener.UpdatedAtListener;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "brand_device_mapping")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
public class BrandDeviceMapping {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "brand_device_mapping_seq")
    @SequenceGenerator(name = "brand_device_mapping_seq", sequenceName = "brand_device_mapping_seq", allocationSize = 1)
    @Column(name = "seq")
    private Long seq;

    @Enumerated(EnumType.STRING)
    @Column(name = "device_category_code")
    private DeviceCategoryCode deviceCategoryCode;

    @ManyToOne(fetch = FetchType.LAZY )
    @JoinColumn(name = "brand_id", referencedColumnName = "brand_id")
    private Brand brandEntity;

    @Enumerated(EnumType.STRING)
    @Column(name = "brand_code")
    private BrandCode brandCode;

    @Column(name = "order_idx")
    private Integer orderIdx;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "device_category_id", referencedColumnName = "device_category_id")
    private DeviceCategory deviceCategoryEntity;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at")
    private Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_at")
    private Date updatedAt;
}
