package com.dike.modules.domain.device.handler.vo.compare.watch;


import com.dike.modules.domain.device.handler.vo.compare.DeviceCompareAttribute;
import com.dike.modules.domain.device.handler.vo.compare.DeviceCompareAttributeResponse;
import com.dike.modules.domain.device.handler.vo.compare.DeviceCompareAttributeVo;
import com.dike.modules.domain.enums.DikeEnumDto;
import com.google.api.client.util.Lists;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WatchCompareAttributeVo extends DeviceCompareAttributeVo {

    private Set<Double> screenSizes;
    private Set<String> materialTypes;
    private Set<DikeEnumDto> networkTypes;

    public DeviceCompareAttributeResponse from() {
        List<DeviceCompareAttribute> attributes = Lists.newArrayList();

        attributes.add(DeviceCompareAttributeVo.getScreenSizeItems(this.screenSizes, "mm"));
        attributes.add(DeviceCompareAttributeVo.getMaterialTypeItems(this.materialTypes));
        attributes.add(DeviceCompareAttributeVo.getNetworkTypeItems(this.networkTypes));


        return DeviceCompareAttributeResponse.builder()
                .attributes(attributes)
                .build();
    }
}
