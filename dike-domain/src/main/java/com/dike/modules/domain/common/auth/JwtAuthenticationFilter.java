package com.dike.modules.domain.common.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private final JwtTokenProvider tokenProvider;


    public static final String ACCESS_TOKEN="Authorization";
    public static final String REFRESH_TOKEN="REFRESH_TOKEN";


    public JwtAuthenticationFilter(JwtTokenProvider provider) {
        this.tokenProvider = provider;
    }


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws BadCredentialsException,ServletException, IOException {
        String jwt = resolveToken(request);

        // token이 유효한지 확인
        if (StringUtils.hasText(jwt) && tokenProvider.validateToken(jwt)) {
            Authentication authentication = tokenProvider.getAccessAuthentication(jwt);
            SecurityContextHolder.getContext().setAuthentication(authentication);       // token에 authentication 정보 삽입
        }

        filterChain.doFilter(request, response);
    }

    private String resolveToken(HttpServletRequest request) {
        String bearerToken = request.getHeader(ACCESS_TOKEN);                            // Authorization 헤더 꺼냄
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {     // JWT 토큰이 존재하는지 확인
            return bearerToken.substring(7);           // "Bearer"를 제거한 accessToken 반환
        }
        return null;
    }
}
