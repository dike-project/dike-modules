package com.dike.modules.domain.device.dsl;


import com.dike.modules.domain.enums.device.BrandCode;

/**
 * Class       : DeviceRepoDSL
 * Author      : 조 준 희
 * Description : Class Description
 * History     : [2022-09-21] - 조 준희 - Class Create
 */
public interface DeviceRepoDSL {
    void setUserDevice(String userId, BrandCode brandCode, String deviceType, String series, String reviewKeywords, int buyYear );
}
