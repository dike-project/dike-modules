package com.dike.modules.domain.cms.BrandDevice;

import com.dike.modules.domain.enums.device.BrandCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@AllArgsConstructor
@Builder
public class BrandResponse {
    private Long brandId;
    private BrandCode brandCode;
    private String brandName;
    private String brandNameEng;
    private String brandIcon;
}
