package com.dike.modules.domain.device.common.dto;

import com.dike.modules.domain.enums.device.DeviceCategoryCode;
import lombok.*;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReviewKeywordDto {
    private Long id;
    private DeviceCategoryCode deviceCategoryCode;
    private Long orderIdx;
    private String reviewKeyword;
}
