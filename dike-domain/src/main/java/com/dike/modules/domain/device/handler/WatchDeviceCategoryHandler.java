package com.dike.modules.domain.device.handler;

import com.dike.modules.domain.device.common.dto.SearchDeviceResponseDto;
import com.dike.modules.domain.device.common.dto.compare.CompareDeviceListDto;
import com.dike.modules.domain.device.handler.condition.WatchDeviceInfoCondition;
import com.dike.modules.domain.device.handler.vo.compare.DeviceCompareAttributeVo;
import com.dike.modules.domain.device.handler.vo.compare.DeviceInfoVo;
import com.dike.modules.domain.device.handler.vo.compare.watch.WatchCompareAttributeVo;
import com.dike.modules.domain.device.watch.WatchRepository;
import com.dike.modules.domain.enums.DikeEnumDto;
import com.dike.modules.domain.enums.device.BrandCode;
import com.dike.modules.domain.enums.device.DeviceCategoryCode;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static com.dike.modules.domain.enums.device.DeviceCategoryCode.WATCH;

@Component
@RequiredArgsConstructor
public class WatchDeviceCategoryHandler implements DeviceCategoryHandler {

    private final WatchRepository watchRepository;

    @Override
    public boolean isSupport(DeviceCategoryCode deviceCategoryCode) {
        return deviceCategoryCode.equals(WATCH);
    }

    @Override
    public List<DeviceInfoVo> getDeviceInfoVos(CompareDeviceListDto compareDeviceListDto) {
        return compareDeviceListDto.getCompareDevices().stream()
                .map(compareDeviceDto -> watchRepository.getWatchDeviceInfoVo(WatchDeviceInfoCondition.from(compareDeviceDto)))
                .collect(Collectors.toList());
    }

    @Override
    public DeviceCompareAttributeVo getDeviceAttributeVo(BrandCode brandCode,
                                                         String modelSeriesName) {
        Set<Double> screenSizes = new HashSet<>();
        Set<String> materialTypes = new HashSet<>();
        Set<DikeEnumDto> networkTypes = new HashSet<>();

        watchRepository.getWatchAttributeDataVos(brandCode, modelSeriesName).forEach(
                vo -> {

                    if (Objects.nonNull(vo.getScreenMM())) {
                        screenSizes.add(vo.getScreenMM());
                    }

                    if (Strings.isNotBlank(vo.getMaterialType())) {
                        materialTypes.add(vo.getMaterialType());
                    }

                    if (Objects.nonNull(vo.getNetworkType())) {
                        networkTypes.add(DikeEnumDto.from(vo.getNetworkType()));
                    }
                }
        );

        return WatchCompareAttributeVo.builder()
                .screenSizes(screenSizes)
                .materialTypes(materialTypes)
                .networkTypes(networkTypes)
                .build();
    }

    @Override
    public List<SearchDeviceResponseDto> searchDeviceBy(BrandCode brandCode, String targetModelName) {
        return watchRepository.findAllByBrandCodeAndModelNameContaining(brandCode, targetModelName)
                .stream()
                .map(e -> SearchDeviceResponseDto.builder()
                        .deviceId(e.getWatchId())
                        .brandCode(e.getBrandCode())
                        .deviceCategoryCode(WATCH)
                        .deviceModelName(e.getModelName())
                        .build()
                )
                .collect(Collectors.toList());
    }

    @Override
    public List<String> searchDeviceModelSeriesBy(BrandCode brandCode, String searchWord) {
        return watchRepository.findModelSeriesByBrandCodeAndSearchWord(brandCode, searchWord);
    }

    @Override
    public boolean checkDeviceExist(Long deviceId, BrandCode brandCode) {
        return watchRepository.existsByWatchIdAndBrandCode(deviceId, brandCode.getName());
    }

    @Override
    public boolean checkModelSeriesExist(BrandCode brandCode, String modelSeriesName) {
        return watchRepository.existsByBrandCodeAndSeries(brandCode, modelSeriesName);
    }
}
