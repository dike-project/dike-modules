package com.dike.modules.domain.device.handler.condition;

import com.dike.modules.domain.device.common.dto.compare.CompareDeviceDto;
import com.dike.modules.domain.device.common.dto.compare.WatchCompareAttributeDto;
import com.dike.modules.domain.enums.device.BrandCode;
import com.dike.modules.domain.enums.device.NetworkType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WatchDeviceInfoCondition {

    private BrandCode brandCode;
    private String modelSeriesName;
    private Double screenSize;
    private String materialType;
    private NetworkType networkType;

    public static WatchDeviceInfoCondition from(CompareDeviceDto dto) {
        WatchCompareAttributeDto attributeDto = (WatchCompareAttributeDto) dto.getCompareAttributes();
        return WatchDeviceInfoCondition.builder()
                .brandCode(dto.getBrandCode())
                .modelSeriesName(dto.getModelSeriesName())
                .screenSize(attributeDto.getScreenSize())
                .materialType(attributeDto.getMaterialType())
                .networkType(attributeDto.getNetworkType())
                .build();
    }
}
