package com.dike.modules.domain.device.common.dto.compare;


import com.dike.modules.domain.enums.device.BrandCode;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonSubTypes({
        @JsonSubTypes.Type(name = "compareAttributes", value = WatchCompareAttributeDto.class),
})
public class CompareDeviceDto {

    @NotBlank(message = "제품 브랜드 코드는 필수입니다.")
    private BrandCode brandCode;
    @NotBlank(message = "모델 시리즈는 필수입니다.")
    private String modelSeriesName;
    @NotNull(message = "비교 속성 정보는 필수입니다.")
    private CompareAttributeDto compareAttributes;
}
