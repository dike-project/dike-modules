package com.dike.modules.domain.device.handler.condition;

import com.dike.modules.domain.device.common.dto.compare.CompareDeviceDto;
import com.dike.modules.domain.device.common.dto.compare.TabletCompareAttributeDto;
import com.dike.modules.domain.enums.device.BrandCode;
import com.dike.modules.domain.enums.device.NetworkType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TabletDeviceInfoCondition {

    private BrandCode brandCode;
    private String modelSeriesName;
    private Double screenSize;
    private Double storageCapacity;
    private NetworkType networkType;

    public static TabletDeviceInfoCondition from(CompareDeviceDto dto) {
        TabletCompareAttributeDto attributeDto = (TabletCompareAttributeDto) dto.getCompareAttributes();
        return TabletDeviceInfoCondition.builder()
                .brandCode(dto.getBrandCode())
                .modelSeriesName(dto.getModelSeriesName())
                .screenSize(attributeDto.getScreenSize())
                .storageCapacity(attributeDto.getStorageCapacity())
                .networkType(attributeDto.getNetworkType())
                .build();
    }
}
