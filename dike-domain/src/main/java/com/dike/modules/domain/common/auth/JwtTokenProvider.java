package com.dike.modules.domain.common.auth;


import com.dike.modules.domain.auth.dto.UserTokenDto;
import com.dike.modules.domain.auth.user.CustomUserDetails;
import io.jsonwebtoken.*;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import java.security.Key;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

@Component
@Log4j2
public class JwtTokenProvider {


    private final String secret;
    private final long accessTokenExpiryInSeconds;
    private final long refreshTokenExpiryInSeconds;
    private Key key;
//    private static final String AUTHORITIES_KEY = "auth";
    private static final String BEARER_TYPE = "Bearer ";

    @Autowired
    public JwtTokenProvider(@Value("${auth.jwt.secret-key:}")String secret,
                            @Value("${auth.jwt.accessExpireTime:}")long accessTokenExpiryInSeconds,
                            @Value("${auth.jwt.refreshExpireTime:}")long refreshTokenExpiryInSeconds) {
        this.secret = secret;
        this.key = Keys.hmacShaKeyFor(Decoders.BASE64.decode(this.secret));
        this.accessTokenExpiryInSeconds = accessTokenExpiryInSeconds * 1000;
        this.refreshTokenExpiryInSeconds = refreshTokenExpiryInSeconds * 1000;
    }

    /** Authentication정보로 토큰 생성
     * @param authentication
     * @return AccessToken
     */
    public UserTokenDto createTokenFromAuthentication(Authentication authentication)
    {
//        // 권한들 가져오기
//        String authorities = authentication.getAuthorities().stream()
//                .map(GrantedAuthority::getAuthority)
//                .collect(Collectors.joining(","));

        long now = (new Date()).getTime();

        // Access Token 생성
        Date accessTokenExpiresIn = new Date(now + accessTokenExpiryInSeconds);
        String accessToken = Jwts.builder()
                .setSubject(authentication.getName())       // payload "sub": "name"
                //.claim(AUTHORITIES_KEY, authorities)        // payload "auth": "ROLE_USER"
                .setExpiration(accessTokenExpiresIn)        // payload "exp": 1516239022 (예시)
                .signWith(key, SignatureAlgorithm.HS512)    // header "alg": "HS512"
                .compact();

        // Refresh Token 생성
        String refreshToken = Jwts.builder()
                .setExpiration(new Date(now + refreshTokenExpiryInSeconds))
                .signWith(key, SignatureAlgorithm.HS512)
                .compact();

        return UserTokenDto.builder()
                .grantType(BEARER_TYPE)
                .accessToken(BEARER_TYPE + accessToken)
                .refreshToken(BEARER_TYPE + refreshToken)
                .build();
    }

    public Authentication getAccessAuthentication(String accessToken) {
        // 토큰 복호화
        Claims claims = parseClaims(accessToken);

//        if (claims.get(AUTHORITIES_KEY) == null) {
//            throw new RuntimeException("권한 정보가 없는 토큰입니다.");
//        }


        // UserDetails 객체를 만들어서 Authentication 리턴
        CustomUserDetails principal = new CustomUserDetails(claims.getSubject(), "", null);

        return new UsernamePasswordAuthenticationToken(principal, "", null);
    }


    /**
     * 토큰으로 Authentication 객체 생성
     * @return
     */
    public Authentication getRefreshAuthentication(String refreshtoken) throws BadCredentialsException {
        // 토큰 복호화
        Claims claims = parseClaims(refreshtoken);

        // UserDetails 객체를 만들어서 Authentication 리턴
        CustomUserDetails principal = new CustomUserDetails(claims.getSubject(), "", null);
        return new UsernamePasswordAuthenticationToken(principal, "", null);
    }

    /**
     * Token 벨리테이션
     * @param token
     * @return
     */
    public boolean validateToken(String token) {
        try {
            Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token);
            return true;
        } catch (io.jsonwebtoken.security.SecurityException | MalformedJwtException e) {
            log.debug("잘못된 JWT 서명입니다.");
        } catch (ExpiredJwtException e) {
            log.debug("만료된 JWT 토큰입니다.");
        } catch (UnsupportedJwtException e) {
            log.debug("지원되지 않는 JWT 토큰입니다.");
        } catch (IllegalArgumentException e) {
            log.debug("JWT 토큰이 잘못되었습니다.");
        }
        return false;
    }
    private Claims parseClaims(String accessToken) {
        try {
            return Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(accessToken).getBody();
        } catch (ExpiredJwtException e) {
            return e.getClaims();
        }
    }
    public Claims extractClaims(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(this.key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    public long getRemainedExpireTime(String token) {
        Date expiration = extractClaims(token).getExpiration();
        return expiration.getTime() - System.currentTimeMillis();
    }
}
