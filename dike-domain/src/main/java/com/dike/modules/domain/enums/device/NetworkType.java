package com.dike.modules.domain.enums.device;

import com.dike.modules.domain.enums.DikeEnum;

public enum NetworkType implements DikeEnum {
    NET_5("5G"),
    LTE("4G/LTE"),
    BLUETOOTH("bluetooth"),
    WiFi("wifi");

    private String description;

    NetworkType(String description) {
        this.description = description;
    }

    @Override
    public String getName() {
        return this.name();
    }

    @Override
    public String getDescription() {
        return this.description;
    }

}
