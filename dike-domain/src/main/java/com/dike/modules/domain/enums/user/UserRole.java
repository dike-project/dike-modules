package com.dike.modules.domain.enums.user;

import com.dike.modules.domain.enums.DikeEnum;


public enum UserRole implements DikeEnum {
    ROLE_USER("회원"),
    ROLE_ADMIN("관리자");

    UserRole(String description) {
        this.description = description;
    }

    private final String description;

    @Override
    public String getName() {
        return this.name();
    }

    @Override
    public String getDescription() {
        return this.description;
    }

}
