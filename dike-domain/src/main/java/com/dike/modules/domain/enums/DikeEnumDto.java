package com.dike.modules.domain.enums;


import lombok.*;

@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class DikeEnumDto {

    private String name;
    private String description;

    public static DikeEnumDto from(DikeEnum dikeEnum) {
        return DikeEnumDto.builder()
                .name(dikeEnum.getName())
                .description(dikeEnum.getDescription())
                .build();
    }
}
