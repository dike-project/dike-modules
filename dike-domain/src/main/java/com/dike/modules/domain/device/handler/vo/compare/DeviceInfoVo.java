package com.dike.modules.domain.device.handler.vo.compare;

import com.google.common.collect.Lists;
import org.apache.logging.log4j.util.Strings;

import java.util.Collections;
import java.util.List;

public interface DeviceInfoVo {
    static List<String> toList(String value) {
        if (Strings.isNotBlank(value)) {
            return Lists.newArrayList(value.split(","));
        }
        return Collections.emptyList();
    }
}
