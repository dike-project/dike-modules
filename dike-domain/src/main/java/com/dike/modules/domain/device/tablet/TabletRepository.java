package com.dike.modules.domain.device.tablet;

import com.dike.modules.domain.enums.device.BrandCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TabletRepository extends JpaRepository<Tablet, Long>, TabletRepositoryCustom {
    boolean existsByTabletIdAndBrandCode(Long tabletId, String brandCode);

    boolean existsByBrandCodeAndSeries(BrandCode brandCode, String series);

    @Query(value = "SELECT t FROM Tablet t WHERE t.brandCode = :#{#brandCode} AND REPLACE(t.modelName,' ','') LIKE CONCAT('%', :modelName,'%')")
    List<Tablet> findAllByBrandCodeAndModelNameContaining(BrandCode brandCode, String modelName);
}
