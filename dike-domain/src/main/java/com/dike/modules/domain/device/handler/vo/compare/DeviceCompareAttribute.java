package com.dike.modules.domain.device.handler.vo.compare;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DeviceCompareAttribute {

    private String attributeType;
    private String title;
    @JsonInclude
    private String unitType;
    private Set<? extends Object> items;
}
