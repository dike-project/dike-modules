package com.dike.modules.domain.device.mobile;

import com.dike.modules.domain.device.handler.condition.MobileDeviceInfoCondition;
import com.dike.modules.domain.device.handler.vo.compare.mobile.MobileAttributeDataVo;
import com.dike.modules.domain.device.handler.vo.compare.mobile.MobileDeviceInfoVo;
import com.dike.modules.domain.enums.device.BrandCode;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.StringTemplate;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;

import java.util.List;
import java.util.Objects;

import static com.dike.modules.domain.device.handler.vo.compare.mobile.MobileAttributeDataVo.MOBILE_ATTRIBUTE_DATA_VO_PROJECTION;
import static com.dike.modules.domain.device.handler.vo.compare.mobile.MobileDeviceInfoVo.MOBILE_DEVICE_INFO_VO_PROJECTION;

public class MobileRepositoryCustomImpl extends QuerydslRepositorySupport implements MobileRepositoryCustom {

    private final QMobile qMobile = QMobile.mobile;


    public MobileRepositoryCustomImpl() {
        super(Mobile.class);
    }


    @Override
    public List<String> findModelSeriesByBrandCodeAndSearchWord(BrandCode brandCode, String searchWord) {
        StringTemplate series = Expressions.stringTemplate("replace({0},'  ','')", qMobile.series);
        return from(qMobile)
                .where(qMobile.brandCode.eq(brandCode),
                        series.contains(searchWord))
                .select(qMobile.series)
                .distinct()
                .orderBy(qMobile.series.desc())
                .fetch();
    }

    @Override
    public List<MobileAttributeDataVo> getMobileAttributeDataVos(BrandCode brandCode, String modelSeriesName) {
        return from(qMobile)
                .where(qMobile.brandCode.eq(brandCode),
                        qMobile.series.eq(modelSeriesName))
                .select(MOBILE_ATTRIBUTE_DATA_VO_PROJECTION)
                .distinct()
                .fetch();
    }

    @Override
    public MobileDeviceInfoVo getMobileDeviceInfoVo(MobileDeviceInfoCondition condition) {
        return from(qMobile)
                .where(getMobileDeviceInfoConditions(condition))
                .select(MOBILE_DEVICE_INFO_VO_PROJECTION)
                .fetchFirst();
    }

    private BooleanBuilder getMobileDeviceInfoConditions(MobileDeviceInfoCondition condition) {
        BooleanBuilder predicates = new BooleanBuilder();

        predicates.and(qMobile.brandCode.eq(condition.getBrandCode()));
        predicates.and(qMobile.series.eq(condition.getModelSeriesName()));

        if (Objects.nonNull(condition.getStorageCapacity())) {
            predicates.and(qMobile.storageCapacityGB.eq(condition.getStorageCapacity()));
        }

        return predicates;
    }

}
