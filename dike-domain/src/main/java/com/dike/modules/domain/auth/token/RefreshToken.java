package com.dike.modules.domain.auth.token;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.TimeToLive;

@Getter
@Builder
@RedisHash("refreshToken")
@AllArgsConstructor
public class RefreshToken {

    @Id
    private String id /* loginId */;
    private String refreshToken;
    @TimeToLive
    private Long refreshTokenExpiredAt;

    public static RefreshToken create(String loginId, String refreshToken, Long remainedExpireTime) {
        return RefreshToken.builder()
            .id(loginId)
            .refreshToken(refreshToken)
            .refreshTokenExpiredAt(remainedExpireTime / 1000)
            .build();
    }
}
