package com.dike.modules.domain.device;

import com.dike.modules.domain.enums.device.BrandCode;
import com.dike.modules.domain.enums.device.DeviceCategoryCode;
import com.dike.modules.domain.listener.CreatedAtListener;
import com.dike.modules.domain.listener.SystemMetaData;
import com.dike.modules.domain.listener.UpdatedAtListener;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

/**
 * Class       : DikeUserDevice
 * Author      : 조 준 희
 * Description : Class Description
 * History     : [2022-09-26] - 조 준희 - Class Create
 */
@Data
@Builder
@Entity
@Table(name = "dike_user_device", schema = "public")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@EntityListeners({CreatedAtListener.class, UpdatedAtListener.class})
public class DikeUserDevice implements SystemMetaData {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dike_user_device_seq")
    @SequenceGenerator(name = "dike_user_device_seq", sequenceName = "dike_user_device_seq", allocationSize = 1)
    @Column(name = "seq")
    private Long seq;

    @Column(name = "login_id")
    private String loginId;

    @Enumerated(EnumType.STRING)
    @Column(name = "device_category_code")
    private DeviceCategoryCode deviceCategoryCode;

    @Enumerated(EnumType.STRING)
    @Column(name = "brand_code")
    private BrandCode brandCode;

    @Column(name = "series")
    private String series;

    @Column(name = "buy_year")
    private Integer buyYear;

    @Column(name = "review_keywords")
    private String reviewKeywords;

    @Column(name = "grade")
    private Integer grade;

    @Column(name = "review")
    private String review;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at")
    private Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_at")
    private Date updatedAt;

}
