package com.dike.modules.domain.device.handler.vo.compare;

import com.dike.modules.domain.enums.DikeEnumDto;

import java.util.Set;
import java.util.stream.Collectors;

public abstract class DeviceCompareAttributeVo {

    abstract public DeviceCompareAttributeResponse from();

    public static DeviceCompareAttribute getScreenSizeItems(Set<Double> values, String unit) {

        Set<String> items = values.stream().map(Object::toString).collect(Collectors.toSet());
        return DeviceCompareAttribute.builder()
                .attributeType("screenSize")
                .unitType(unit)
                .title("화면크기")
                .items(items)
                .build();
    }

    public static DeviceCompareAttribute getNetworkTypeItems(Set<DikeEnumDto> values) {
        return DeviceCompareAttribute.builder()
                .attributeType("networkType")
                .unitType("enum")
                .title("통신타입")
                .items(values)
                .build();
    }

    public static DeviceCompareAttribute getStorageCapacityItems(Set<? extends Number> values) {
        Set<String> items = values.stream().map(Object::toString).collect(Collectors.toSet());
        return DeviceCompareAttribute.builder()
                .attributeType("storageCapacity")
                .unitType("GB")
                .title("저장용량")
                .items(items)
                .build();
    }

    public static DeviceCompareAttribute getMaterialTypeItems(Set<String> values) {
        return DeviceCompareAttribute.builder()
                .attributeType("materialType")
                .unitType(null)
                .title("소재")
                .items(values)
                .build();
    }


}
