package com.dike.modules.domain.device.handler.vo.compare.tablet;


import com.dike.modules.domain.device.handler.vo.compare.DeviceCompareAttribute;
import com.dike.modules.domain.device.handler.vo.compare.DeviceCompareAttributeResponse;
import com.dike.modules.domain.device.handler.vo.compare.DeviceCompareAttributeVo;
import com.dike.modules.domain.enums.DikeEnumDto;
import com.google.api.client.util.Lists;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TabletCompareAttributeVo extends DeviceCompareAttributeVo {

    private Set<Double> screenSizes;
    private Set<DikeEnumDto> networkTypes;
    private Set<Double> storageCapacities;

    public DeviceCompareAttributeResponse from() {
        List<DeviceCompareAttribute> attributes = Lists.newArrayList();

        attributes.add(DeviceCompareAttributeVo.getScreenSizeItems(this.screenSizes, "inch"));
        attributes.add(DeviceCompareAttributeVo.getStorageCapacityItems(this.storageCapacities));
        attributes.add(DeviceCompareAttributeVo.getNetworkTypeItems(this.networkTypes));


        return DeviceCompareAttributeResponse.builder()
                .attributes(attributes)
                .build();
    }
}
