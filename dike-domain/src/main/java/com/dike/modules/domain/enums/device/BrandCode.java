package com.dike.modules.domain.enums.device;

import com.dike.modules.domain.enums.DikeEnum;
import com.dike.modules.domain.exception.DikeServiceException;
import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;

import static com.dike.modules.domain.common.response.ApiStatusCode.INVALID_PARAMETER;

@Getter
public enum BrandCode implements DikeEnum {

    APPLE("애플", Set.of("애플", "apple", "Apple")),
    SAMSUNG("삼성", Set.of("삼성전자", "삼성", "Samsung")),
    LG("LG", Set.of("LG전자", "LG", "엘쥐", "앨쥐")),
    BOSE("보세", Set.of("보스", "Bose", "bose")),
    DELL("델", Set.of("델", "Dell", "dell")),
    BRITZ("브리츠", Set.of("브리츠", "Britz", "britz", "BritZ")),
    LENOVO("레노버", Set.of("레노버", "Lenovo", "lenovo")),
    SHOKZ("에프터샥", Set.of("샥즈", "샥츠", "shokz", "SHOKZ")),
    XIAOMI("샤오미", Set.of("샤오미", "xiaomi", "Xiaomi", "mi", "MI", "Mi")),
    SONY("소니", Set.of("소니", "쏘니", "sony"));

    // 추후, 제조사를 통한 기기 검색에 활용
    private Set<String> synonyms;
    private String description;

    BrandCode(String description, Set<String> synonyms) {
        this.description = description;
        this.synonyms = synonyms;
    }

    public static BrandCode of(String value) {
        try {
            return BrandCode.valueOf(value);
        } catch (IllegalArgumentException exception) {
            Optional<BrandCode> brandCode = Arrays.stream(BrandCode.values())
                    .filter(code -> code.synonyms.contains(value))
                    .findFirst();

            if (brandCode.isPresent()) {
                return brandCode.get();
            } else {
                throw new DikeServiceException(INVALID_PARAMETER, "알 수 없는 브랜드입니다.");
            }
        }
    }

    @Override
    public String getName() {
        return this.name();
    }

    @Override
    public String getDescription() {
        return this.description;
    }

}
