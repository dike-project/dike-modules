package com.dike.modules.domain.auth.dto;

import lombok.*;

import javax.validation.constraints.NotBlank;


@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserLoginDto {

    @NotBlank(message = "아이디를 입력해주세요.")
    private String loginId;

    @NotBlank(message = "비밀번호를 입력해주세요.")
    private String password;


}
