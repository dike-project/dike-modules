package com.dike.modules.domain.cms.BrandDevice;

import com.dike.modules.domain.enums.device.DeviceCategoryCode;
import com.dike.modules.domain.listener.CreatedAtListener;
import com.dike.modules.domain.listener.UpdatedAtListener;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * Class       : DeviceCategory
 * Author      : 조 준 희
 * Description : Class Description
 * History     : [2022-07-22] - 조 준희 - Class Create
 */
@Entity
@Table(name = "device_category", schema = "public")
@EntityListeners({CreatedAtListener.class, UpdatedAtListener.class})
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DeviceCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "device_category_seq")
    @SequenceGenerator(name = "device_category_seq", sequenceName = "device_category_seq", allocationSize = 1)
    @Column(name = "device_category_id", nullable = false)
    private Long deviceCategoryId;

    @Column(name = "device_category_name", nullable = false)
    private String deviceCategoryName;

    @Enumerated(EnumType.STRING)
    @Column(name = "device_category_code", nullable = false)
    private DeviceCategoryCode deviceCategoryCode;

    @Column(name = "device_category_icon", nullable = false)
    private String deviceCategoryIcon;

    @Column(name = "order_idx", columnDefinition = "999")
    private Integer orderIdx;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at", nullable = false)
    private Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_at", nullable = false)
    private Date updatedAt;


    public DeviceCategoryResponse toResponse(DeviceCategory entity)
    {
        return DeviceCategoryResponse.builder().deviceCategoryId(entity.getDeviceCategoryId())
                .deviceCategoryCode(entity.deviceCategoryCode)
                .deviceCategoryName(entity.getDeviceCategoryName())
                .deviceCategoryIcon(entity.getDeviceCategoryIcon()).build();
    }
}
