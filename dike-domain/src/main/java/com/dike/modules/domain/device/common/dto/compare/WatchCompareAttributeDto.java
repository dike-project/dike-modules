package com.dike.modules.domain.device.common.dto.compare;


import com.dike.modules.domain.enums.device.NetworkType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class WatchCompareAttributeDto extends CompareAttributeDto {

    private Double screenSize;
    private String materialType;
    private NetworkType networkType;

    public WatchCompareAttributeDto(String type,
                                    Double screenSize,
                                    String materialType,
                                    NetworkType networkType) {
        super(type);
        this.screenSize = screenSize;
        this.materialType = materialType;
        this.networkType = networkType;
    }
}
