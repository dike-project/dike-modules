package com.dike.modules.domain.device.common.dto.compare;


import com.dike.modules.domain.enums.device.NetworkType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class TabletCompareAttributeDto extends CompareAttributeDto {

    private Double screenSize;
    private NetworkType networkType;
    private Double storageCapacity;

    public TabletCompareAttributeDto(String type,
                                     Double screenSize,
                                     NetworkType networkType,
                                     Double storageCapacity) {
        super(type);
        this.screenSize = screenSize;
        this.networkType = networkType;
        this.storageCapacity = storageCapacity;
    }
}
