package com.dike.modules.domain.device.handler;

import com.dike.modules.domain.device.common.dto.SearchDeviceResponseDto;
import com.dike.modules.domain.device.common.dto.compare.CompareDeviceListDto;
import com.dike.modules.domain.device.handler.condition.TabletDeviceInfoCondition;
import com.dike.modules.domain.device.handler.vo.compare.DeviceCompareAttributeVo;
import com.dike.modules.domain.device.handler.vo.compare.DeviceInfoVo;
import com.dike.modules.domain.device.handler.vo.compare.tablet.TabletCompareAttributeVo;
import com.dike.modules.domain.device.tablet.TabletRepository;
import com.dike.modules.domain.enums.DikeEnumDto;
import com.dike.modules.domain.enums.device.BrandCode;
import com.dike.modules.domain.enums.device.DeviceCategoryCode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static com.dike.modules.domain.enums.device.DeviceCategoryCode.TABLET;

@Component
@RequiredArgsConstructor
public class TabletDeviceCategoryHandler implements DeviceCategoryHandler {

    private final TabletRepository tabletRepository;

    @Override
    public boolean isSupport(DeviceCategoryCode deviceCategoryCode) {
        return deviceCategoryCode.equals(TABLET);
    }

    @Override
    public List<DeviceInfoVo> getDeviceInfoVos(CompareDeviceListDto compareDeviceListDto) {
        return compareDeviceListDto.getCompareDevices().stream()
                .map(compareDeviceDto -> tabletRepository.getTabletDeviceInfoVo(TabletDeviceInfoCondition.from(compareDeviceDto)))
                .collect(Collectors.toList());
    }

    @Override
    public DeviceCompareAttributeVo getDeviceAttributeVo(BrandCode brandCode, String modelSeriesName) {
        Set<Double> screenSizes = new HashSet<>();
        Set<Double> storageCapacities = new HashSet<>();
        Set<DikeEnumDto> networkTypes = new HashSet<>();

        tabletRepository.getTabletAttributeDataVos(brandCode, modelSeriesName).forEach(
                vo -> {

                    if (Objects.nonNull(vo.getScreenInch())) {
                        screenSizes.add(vo.getScreenInch());
                    }

                    if (Objects.nonNull(vo.getStorageCapacityGB())) {
                        storageCapacities.add(vo.getStorageCapacityGB());
                    }

                    if (Objects.nonNull(vo.getNetworkType())) {
                        networkTypes.add(DikeEnumDto.from(vo.getNetworkType()));
                    }
                }
        );

        return TabletCompareAttributeVo.builder()
                .screenSizes(screenSizes)
                .storageCapacities(storageCapacities)
                .networkTypes(networkTypes)
                .build();
    }

    @Override
    public List<SearchDeviceResponseDto> searchDeviceBy(BrandCode brandCode, String targetModelName) {
        return tabletRepository.findAllByBrandCodeAndModelNameContaining(brandCode, targetModelName).stream()
                .map(e -> SearchDeviceResponseDto.builder()
                        .deviceId(e.getTabletId())
                        .brandCode(e.getBrandCode())
                        .deviceCategoryCode(TABLET)
                        .deviceModelName(e.getSeries())
                        .build()
                )
                .collect(Collectors.toList());
    }

    @Override
    public List<String> searchDeviceModelSeriesBy(BrandCode brandCode, String searchWord) {
        return tabletRepository.findModelSeriesByBrandCodeAndSearchWord(brandCode, searchWord);
    }

    @Override
    public boolean checkDeviceExist(Long deviceId, BrandCode brandCode) {
        return tabletRepository.existsByTabletIdAndBrandCode(deviceId, brandCode.getName());
    }

    @Override
    public boolean checkModelSeriesExist(BrandCode brandCode, String modelSeriesName) {
        return tabletRepository.existsByBrandCodeAndSeries(brandCode, modelSeriesName);
    }
}
