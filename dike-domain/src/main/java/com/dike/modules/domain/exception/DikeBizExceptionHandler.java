package com.dike.modules.domain.exception;

import com.dike.modules.domain.common.response.ApiStatusCode;
import com.dike.modules.domain.common.response.ResponseJsonObject;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.stream.Collectors;

/**
 * Description : 디케 서비스 에서 발생되는 API Exception을 핸들링 한다.
 * Author      : 조 준 희
 * History     : [2023-06-01] - 조 준 희 - Create
 */
@Log4j2
@RestControllerAdvice
public class DikeBizExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * Description : 서비스 실행 중 치명적인 Runtime 에러 발생 시 핸들링.
     * Author      : 조 준 희
     * History     : [2023-06-01] - 조 준 희 - Create
     */
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ResponseJsonObject> handleException(Exception ex) {
        log.error("RuntimeExceptionHandler : {} \n StackTrace : " , ex.getMessage(), ex.getStackTrace());
        ResponseJsonObject response = ResponseJsonObject.builder().code(ApiStatusCode.SYSTEM_ERROR).msg(ex.toString()).build();

        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }


    /**
     * Description : @RequestBody, @RequestHeader의 매핑 실패일경우  ConstraintViolationException 예외 에러가 발생한다.
     * Author      : 조 준 희
     * History     : [2023-06-01] - 조 준 희 - Create
     */
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ResponseJsonObject> handleConstraintViolationException(ConstraintViolationException ex) {
        log.error("파라미터 유효성 체크 실패. : {} \n StackTrace : " , ex.getMessage(), ex.getStackTrace());

        ResponseJsonObject response;

        if(ex.getConstraintViolations().isEmpty() == false)
        {
            String exceptionMsg = ex.getConstraintViolations().stream()
                    .map(v1 -> v1.getMessage())
                    .collect(Collectors.joining(","));
            response = ResponseJsonObject.builder().code(ApiStatusCode.PARAMETER_CHECK_FAILED).msg(exceptionMsg).build();
        }
        else
            response = ResponseJsonObject.builder().code(ApiStatusCode.PARAMETER_CHECK_FAILED).build();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    /**
     * Description : @RequestBody의 매핑 실패일경우  MethodArgumentNotValidException 예외 에러가 발생한다.
     * Author      : 조 준 희
     * History     : [2023-06-01] - 조 준 희 - Create
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status, WebRequest request) {
        log.error("파라미터 유효성 체크 실패. : {} \n StackTrace : " , ex.getMessage(), ex.getStackTrace());
        ResponseJsonObject response;

        if(ex.getBindingResult().hasErrors())
            response = ResponseJsonObject.builder().code(ApiStatusCode.PARAMETER_CHECK_FAILED)
                .msg(ex.getBindingResult().getFieldError().getDefaultMessage()).build();
        else
            response = ResponseJsonObject.builder().code(ApiStatusCode.PARAMETER_CHECK_FAILED).build();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    /**
     * Description : 디케 서비스 사용자 정의 에러.
     *  - 디케 서비스 사용자 정의 에러의 코드별로 HTTP Status의 상태 코드도 변경하고 응답처리.
     * Author      : 조 준 희
     * History     : [2023-06-01] - 조 준 희 - Create
     */
    @ExceptionHandler({DikeServiceException.class})
    public ResponseEntity<ResponseJsonObject> handleDikeServiceException(DikeServiceException ex) {
        log.debug("DikeServiceExceptionHandler : {}" , ex.getResponseJsonObject().toString());
        HttpStatus exceptionStatus = HttpStatus.OK;

        switch (ex.getResponseJsonObject().getCode()){
            case 400:
                exceptionStatus = HttpStatus.BAD_REQUEST;
                break;
            case 401:
                exceptionStatus = HttpStatus.UNAUTHORIZED;
                break;
            case 403:
                exceptionStatus = HttpStatus.FORBIDDEN;
                break;
            case 500:
                exceptionStatus = HttpStatus.INTERNAL_SERVER_ERROR;
                break;
            default:
                exceptionStatus = HttpStatus.OK;
        }

        return new ResponseEntity<>(ex.getResponseJsonObject(), exceptionStatus);
    }
}
