package com.dike.modules.domain.device.handler.vo.compare.mobile;


import com.dike.modules.domain.device.handler.vo.compare.DeviceCompareAttribute;
import com.dike.modules.domain.device.handler.vo.compare.DeviceCompareAttributeResponse;
import com.dike.modules.domain.device.handler.vo.compare.DeviceCompareAttributeVo;
import com.google.api.client.util.Lists;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MobileCompareAttributeVo extends DeviceCompareAttributeVo {

    private Set<Integer> storageCapacities;

    public DeviceCompareAttributeResponse from() {
        List<DeviceCompareAttribute> attributes = Lists.newArrayList();

        attributes.add(DeviceCompareAttributeVo.getStorageCapacityItems(this.storageCapacities));

        return DeviceCompareAttributeResponse.builder()
                .attributes(attributes)
                .build();
    }

}
