package com.dike.modules.domain.auth.token;


import org.springframework.data.repository.CrudRepository;

public interface PasswordRecoverTokenRepository extends CrudRepository<PasswordRecoverToken, String> {
}
