package com.dike.modules.domain.auth.token;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.TimeToLive;

@Getter
@Builder
@RedisHash("logoutToken")
@AllArgsConstructor
public class LogoutToken {

    @Id
    private String id /* accessToken */;

    private String loginId;

    @TimeToLive
    private Long expiration;

    public static LogoutToken of(String accessToken, String loginId, Long remainedExpireTime) {
        return LogoutToken.builder()
                .id(accessToken)
                .loginId(loginId)
                .expiration(remainedExpireTime / 1000)
                .build();
    }
}
