package com.dike.modules.domain.device.common.dto.compare;


import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "type")   // field on which we differentiate objects
@JsonSubTypes({
        @JsonSubTypes.Type(value = WatchCompareAttributeDto.class, name = "WATCH"),
        @JsonSubTypes.Type(value = MobileCompareAttributeDto.class, name = "MOBILE"),
        @JsonSubTypes.Type(value = TabletCompareAttributeDto.class, name = "TABLET"),
})
public abstract class CompareAttributeDto {
    private String type;

    public CompareAttributeDto(String type) {
        this.type = type;
    }

    public CompareAttributeDto() {
    }

}
