package com.dike.modules.domain.cms.youtube;

import com.dike.modules.domain.cms.youtube.dto.YouTubeDto;
import com.dike.modules.domain.cms.youtube.dto.YoutubeRecommendResponse;
import com.dike.modules.domain.cms.youtube.entity.YoutubeRecommend;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.Channel;
import com.google.api.services.youtube.model.Thumbnail;
import com.google.api.services.youtube.model.Video;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

@Service
public class YouTubeApiService {
    private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
    private static final JacksonFactory JSON_FACTORY = new JacksonFactory();
    private static final long NUMBER_OF_VIDEOS_RETURNED = 1;
    private static YouTube youtube;
    private YoutubeRecommendRepository youtubeRecommendRepository;

    @Autowired
    public YouTubeApiService(YoutubeRecommendRepository youtubeRecommendRepository) {
        this.youtubeRecommendRepository = youtubeRecommendRepository;
    }

    public List<YoutubeRecommendResponse> YoutubeVideos() {

        try {

            List<YoutubeRecommend> youtubeRecommends = youtubeRecommendRepository.findAllByOrderByOrderIdxAsc();

            // 유튜브 추천 데이터 미 존재 시 Null 리턴.
            if(youtubeRecommends.stream().count() == 0)
                return null;

            List<YoutubeRecommendResponse> response = new ArrayList<YoutubeRecommendResponse>();
            String channelIds = "";


            youtube = new YouTube.Builder(HTTP_TRANSPORT, JSON_FACTORY, new HttpRequestInitializer() {
                public void initialize(HttpRequest request) throws IOException {
                }
            }).setApplicationName("youtube-video-duration-get").build();

            for(YoutubeRecommend recommend : youtubeRecommends ) {
                List<YouTubeDto> youTubeDtos = new ArrayList<>();
                YoutubeRecommendResponse category = YoutubeRecommendResponse.builder()
                        .youtubeCategoryCode(recommend.getCategoryCode())
                        .youtubeCategoryName(recommend.getCategoryName())
                        .build();

                YouTube.Videos.List videos = youtube.videos().list("id,snippet,contentDetails");
                videos.setKey("AIzaSyCi-S47mjwlbn4pENPMmexzuewKKvAX0_E");
                videos.setId(recommend.getVideos());
                videos.setMaxResults(NUMBER_OF_VIDEOS_RETURNED); //조회 최대 갯수.
                List<Video> videoList = videos.execute().getItems();

                if (videoList == null) {
                    return null;
                }

                Iterator<Video> iteratorSearchResults = videoList.iterator();

                if (!iteratorSearchResults.hasNext()) {
                    System.out.println(" There aren't any results for your query.");
                }
                while (iteratorSearchResults.hasNext()) {

                    Video singleVideo = iteratorSearchResults.next();

                    // Double checks the kind is video.
                    if (singleVideo.getKind().equals("youtube#video")) {

                        YouTubeDto youTubeDto = new YouTubeDto();
                        Thumbnail thumbnail = (Thumbnail) singleVideo.getSnippet().getThumbnails().get("high");


                        youTubeDto.setThumbnailPath(thumbnail.getUrl());
                        youTubeDto.setVideoTitle(singleVideo.getSnippet().getTitle());
                        youTubeDto.setChannelTitle(singleVideo.getSnippet().getChannelTitle());
                        youTubeDto.setChannelId(singleVideo.getSnippet().getChannelId());
                        youTubeDto.setVideoId(singleVideo.getId());
                        youTubeDto.setEmbed("https://www.youtube.com/embed/" + singleVideo.getId());

                        youTubeDtos.add(youTubeDto);
                        if (channelIds.equals("") == false)
                            channelIds += ",";
                        channelIds += singleVideo.getSnippet().getChannelId();
                    }
                }

                category.setYoutubeList(youTubeDtos);

                response.add( category);
            }

            // 중복 없는 모든 채널 썸네일 찾기.
            HashMap<String,String> channelThums = getChannelThum(channelIds);

            for(int cateIdx = 0 ; cateIdx < response.stream().count() ; cateIdx++)
                for(int i = 0 ; i < response.get(cateIdx).getYoutubeList().stream().count() ; i++)
                    response.get(cateIdx).getYoutubeList().get(i).setChannelThumbnail(channelThums.get(response.get(cateIdx).getYoutubeList().get(i).getChannelId()));

            return response;

        } catch (GoogleJsonResponseException e) {
            System.err.println("There was a service error: " + e.getDetails().getCode() + " : "
                    + e.getDetails().getMessage());
        } catch (IOException e) {
            System.err.println("There was an IO error: " + e.getCause() + " : " + e.getMessage());
        } catch (Throwable t) {
            t.printStackTrace();
        }

        return null;
    }


    public HashMap<String,String> getChannelThum(String ids) {


        try {

            //내가 원하는 정보 지정할 수 있어요. 공식 API문서를 참고해주세요.
            YouTube.Channels.List channel = youtube.channels().list("id,snippet");
            channel.setKey("AIzaSyCi-S47mjwlbn4pENPMmexzuewKKvAX0_E");
            channel.setId(ids);
            channel.setMaxResults(NUMBER_OF_VIDEOS_RETURNED); //조회 최대 갯수.
            List<Channel> channelList = channel.execute().getItems();

            if (channelList == null) {
                return null;
            }

            Iterator<Channel> iteratorSearchResults = channelList.iterator();

            if (!iteratorSearchResults.hasNext()) {
                System.out.println(" There aren't any results for your query.");
            }


            HashMap<String,String> result = new HashMap<>();
            while (iteratorSearchResults.hasNext()) {

                Channel singleChannel = iteratorSearchResults.next();

                // Double checks the kind is video.
                if (singleChannel.getKind().equals("youtube#channel")) {

                    result.put(singleChannel.getId(),singleChannel.getSnippet().getThumbnails().getDefault().getUrl());

                }
            }

            return result;

        } catch (GoogleJsonResponseException e) {
            System.err.println("There was a service error: " + e.getDetails().getCode() + " : "
                    + e.getDetails().getMessage());
        } catch (IOException e) {
            System.err.println("There was an IO error: " + e.getCause() + " : " + e.getMessage());
        } catch (Throwable t) {
            t.printStackTrace();
        }

        return null;
    }
}