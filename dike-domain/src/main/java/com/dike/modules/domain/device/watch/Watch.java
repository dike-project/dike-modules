package com.dike.modules.domain.device.watch;

import com.dike.modules.domain.enums.device.BrandCode;
import com.dike.modules.domain.enums.device.NetworkType;
import com.dike.modules.domain.listener.CreatedAtListener;
import com.dike.modules.domain.listener.SystemMetaData;
import com.dike.modules.domain.listener.UpdatedAtListener;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Data
@Builder
@Entity
@Table(name = "watch", schema = "public")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@EntityListeners({CreatedAtListener.class, UpdatedAtListener.class})
public class Watch implements SystemMetaData {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "watch_seq")
    @SequenceGenerator(name = "watch_seq", sequenceName = "watch_seq", allocationSize = 1)
    @Column(name = "watch_id")
    private Long watchId;

    @Enumerated(EnumType.STRING)
    @Column(name = "brand_code")
    private BrandCode brandCode;

    @Column(name = "series")
    private String series;

    @Column(name = "registered_at")
    @Temporal(TemporalType.DATE)
    private Date registeredAt;

    @Column(name = "device_shape")
    private String deviceShape;

    @Column(name = "screen_resolution")
    private String screenResolution;

    @Column(name = "screen_mm")
    private Double screenMM;

    @Column(name = "screen_inch")
    private Double screenInch;

    @Column(name = "ram_gb")
    private Double ramGB;

    @Column(name = "material_type")
    private String materialType;

    @Column(name = "model_name")
    private String modelName;

    @Column(name = "image_path")
    private String imagePath;

    @Column(name = "original_price")
    private Integer originalPrice;

    @Column(name = "colors")
    private String colors;

    @Column(name = "weight_g")
    private Double weightG;

    @Column(name = "exercise_support")
    private String exerciseSupport;

    @Column(name = "health_support")
    private String healthSupport;

    @Column(name = "battery_time_hour")
    private Integer batteryTimeHour;

    @Column(name = "loss_prevention")
    private Boolean lossPrevention;

    @Column(name = "wireless_charging")
    private Boolean wirelessCharging;

    @Column(name = "alarm_support")
    private String alarmSupport;

    @Column(name = "ai_voice_recognition")
    private Boolean aiVoiceRecognition;

    @Column(name = "water_dust_proof")
    private String waterDustProof;

    @Enumerated(EnumType.STRING)
    @Column(name = "network_type")
    private NetworkType networkType;

    @Column(name = "gps")
    private Boolean gps;

    @Column(name = "nfc")
    private Boolean nfc;

    @Column(name = "callable")
    private Boolean callable;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at")
    private Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_at")
    private Date updatedAt;

}
