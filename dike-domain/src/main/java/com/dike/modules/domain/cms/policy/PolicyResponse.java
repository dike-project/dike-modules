package com.dike.modules.domain.cms.policy;

import com.dike.modules.domain.cms.BrandDevice.BrandResponse;
import com.dike.modules.domain.listener.CreatedAtListener;
import com.dike.modules.domain.listener.UpdatedAtListener;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * Class       : Brand
 * Author      : 조 준 희
 * Description : Class Description
 * History     : [2022-07-22] - 조 준희 - Class Create
 */
@Getter
@AllArgsConstructor
@Builder
public class PolicyResponse {
    private String policyId;
    private String policyName;
    private Boolean isOptional;
    private String link;
}
