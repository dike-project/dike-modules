package com.dike.modules.domain.device.laptop;

import com.dike.modules.domain.enums.device.BrandCode;
import com.dike.modules.domain.enums.device.ChargingType;
import com.dike.modules.domain.listener.CreatedAtListener;
import com.dike.modules.domain.listener.SystemMetaData;
import com.dike.modules.domain.listener.UpdatedAtListener;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Data
@Builder
@Entity
@Table(name = "laptop", schema = "public")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@EntityListeners({CreatedAtListener.class, UpdatedAtListener.class})
public class Laptop implements SystemMetaData {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "laptop_seq")
    @SequenceGenerator(name = "laptop_seq", sequenceName = "laptop_seq", allocationSize = 1)
    @Column(name = "laptop_id")
    private Long laptopId;

    @Enumerated(EnumType.STRING)
    @Column(name = "brand_code")
    private BrandCode brandCode;

    @Column(name = "registered_at")
    @Temporal(TemporalType.DATE)
    private Date registeredAt;

    @Column(name = "screen_inch")
    private Double screenInch;

    @Column(name = "screen_resolution")
    private String screenResolution;

    @Column(name = "thickness_mm")
    private Double thicknessMM;

    @Column(name = "weight_kg")
    private Double weightKg;

    @Column(name = "cpu_manufacturer")
    private String cpuManufacturer;

    @Column(name = "cpu_type")
    private String cpuType;

    @Column(name = "cpu_number")
    private String cpuNumber;

    @Column(name = "core_type")
    private String coreType;

    @Column(name = "ram_gb")
    private Double ramGB;

    @Column(name = "ram_exchangeable")
    private Boolean ramExchangeable;

    @Column(name = "storage_capacity_gb")
    private Integer storageCapacityGB;

    @Column(name = "gpu_built_in")
    private Boolean gpuBuiltIn;

    @Column(name = "gpu_manufacturer")
    private String gpuManufacturer;

    @Column(name = "gpu_memory_gb")
    private Integer gpuMemoryGB;

    @Column(name = "hdmi_port")
    private Boolean hdmiPort;

    @Column(name = "usb_port_number")
    private Integer usbPortNumber;

    @Column(name = "usb_type_c")
    private Boolean usbTypeC;

    @Column(name = "usb_type_a")
    private Boolean usbTypeA;

    @Column(name = "micro_sd_card")
    private Boolean microSDCard;

    @Column(name = "security")
    private String security;

    @Enumerated(EnumType.STRING)
    @Column(name = "charging_type")
    private ChargingType chargingType;

    @Column(name = "purpose")
    private String purpose;

    @Column(name = "model_code")
    private String modelCode;

    @Column(name = "model_name")
    private String modelName;

    @Column(name = "image_path")
    private String imagePath;

    @Column(name = "original_price")
    private Integer originalPrice;

    @Column(name = "colors")
    private String colors;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at")
    private Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_at")
    private Date updatedAt;
}
