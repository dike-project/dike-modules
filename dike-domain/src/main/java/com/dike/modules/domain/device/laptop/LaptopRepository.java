package com.dike.modules.domain.device.laptop;

import com.dike.modules.domain.device.watch.Watch;
import com.dike.modules.domain.enums.device.BrandCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LaptopRepository extends JpaRepository<Laptop, Long> {
    boolean existsByLaptopIdAndBrandCode(Long laptopId, String brandCode);

    @Query(value = "SELECT m FROM Laptop m WHERE m.brandCode = :#{#brandCode} AND REPLACE(m.modelName,' ','') LIKE CONCAT('%', :modelName,'%')")
    List<Laptop> findAllByBrandCodeAndModelNameContaining(BrandCode brandCode, String modelName);

}
