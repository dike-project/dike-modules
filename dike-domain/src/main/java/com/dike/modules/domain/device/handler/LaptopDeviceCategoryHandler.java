package com.dike.modules.domain.device.handler;

import com.dike.modules.domain.device.common.dto.SearchDeviceResponseDto;
import com.dike.modules.domain.device.common.dto.compare.CompareDeviceListDto;
import com.dike.modules.domain.device.handler.vo.compare.DeviceCompareAttributeVo;
import com.dike.modules.domain.device.handler.vo.compare.DeviceInfoVo;
import com.dike.modules.domain.device.laptop.LaptopRepository;
import com.dike.modules.domain.enums.device.BrandCode;
import com.dike.modules.domain.enums.device.DeviceCategoryCode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

import static com.dike.modules.domain.enums.device.DeviceCategoryCode.LAPTOP;

@Component
@RequiredArgsConstructor
public class LaptopDeviceCategoryHandler implements DeviceCategoryHandler {

    private final LaptopRepository laptopRepository;

    @Override
    public boolean isSupport(DeviceCategoryCode deviceCategoryCode) {
        return deviceCategoryCode.equals(LAPTOP);
    }

    @Override
    public List<DeviceInfoVo> getDeviceInfoVos(CompareDeviceListDto compareDeviceListDto) {
        return null;
    }

    @Override
    public DeviceCompareAttributeVo getDeviceAttributeVo(BrandCode brandCode, String modelSeriesName) {
        return null;
    }

    @Override
    public List<SearchDeviceResponseDto> searchDeviceBy(BrandCode brandCode, String targetModelName) {
        return laptopRepository.findAllByBrandCodeAndModelNameContaining(brandCode, targetModelName).stream()
                .map(e -> SearchDeviceResponseDto.builder()
                        .deviceId(e.getLaptopId())
                        .brandCode(e.getBrandCode())
                        .deviceCategoryCode(LAPTOP)
                        .deviceModelName(e.getModelName())
                        .build()
                )
                .collect(Collectors.toList());
    }

    @Override
    public List<String> searchDeviceModelSeriesBy(BrandCode brandCode, String searchWord) {
        return null;
    }

    @Override
    public boolean checkDeviceExist(Long deviceId, BrandCode brandCode) {
        return laptopRepository.existsByLaptopIdAndBrandCode(deviceId, brandCode.getName());
    }

    @Override
    public boolean checkModelSeriesExist(BrandCode brandCode, String modelSeriesName) {
        return false;
    }
}
