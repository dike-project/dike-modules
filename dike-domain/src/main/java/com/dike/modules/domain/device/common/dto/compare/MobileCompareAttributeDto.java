package com.dike.modules.domain.device.common.dto.compare;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class MobileCompareAttributeDto extends CompareAttributeDto {

    private Integer storageCapacity;

    public MobileCompareAttributeDto(String type, Integer storageCapacity) {
        super(type);
        this.storageCapacity = storageCapacity;
    }
}
