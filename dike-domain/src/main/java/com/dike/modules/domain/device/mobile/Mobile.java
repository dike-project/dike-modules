package com.dike.modules.domain.device.mobile;


import com.dike.modules.domain.enums.device.BrandCode;
import com.dike.modules.domain.enums.device.ChargingType;
import com.dike.modules.domain.enums.device.NetworkType;
import com.dike.modules.domain.listener.CreatedAtListener;
import com.dike.modules.domain.listener.SystemMetaData;
import com.dike.modules.domain.listener.UpdatedAtListener;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Data
@Builder
@Entity
@Table(name = "mobile", schema = "public")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@EntityListeners({CreatedAtListener.class, UpdatedAtListener.class})
public class Mobile implements SystemMetaData {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mobile_seq")
    @SequenceGenerator(name = "mobile_seq", sequenceName = "mobile_seq", allocationSize = 1)
    @Column(name = "mobile_id")
    private Long mobileId;

    @Enumerated(EnumType.STRING)
    @Column(name = "brand_code")
    private BrandCode brandCode;

    @Column(name = "series")
    private String series;

    @Column(name = "model_name")
    private String modelName;

    @Column(name = "model_code")
    private String modelCode;

    @Column(name = "registered_at")
    @Temporal(TemporalType.DATE)
    private Date registeredAt;

    @Column(name = "screen_inch")
    private Double screenInch;

    @Column(name = "screen_resolution")
    private String screenResolution;

    @Column(name = "camera_type")
    private String cameraType;

    @Column(name = "rear_camera")
    private String rearCamera;

    @Column(name = "front_camera")
    private String frontCamera;

    @Column(name = "water_dust_proof")
    private String waterDustProof;

    @Enumerated(EnumType.STRING)
    @Column(name = "charging_type")
    private ChargingType chargingType;

    @Column(name = "weight_g")
    private Double weightG;

    @Column(name = "thickness_mm")
    private Double thicknessMM;

    @Column(name = "screen_width_mm")
    private Double screenWidthMM;

    @Column(name = "screen_height_mm")
    private Double screenHeightMM;

    @Enumerated(EnumType.STRING)
    @Column(name = "network_type")
    private NetworkType networkType;

    @Column(name = "ram_gb")
    private Integer ramGB;

    @Column(name = "storage_capacity_gb")
    private Integer storageCapacityGB;

    @Column(name = "security")
    private String security;

    @Column(name = "image_path")
    private String imagePath;

    @Column(name = "original_price")
    private Integer originalPrice;

    @Column(name = "colors")
    private String colors;

    @Column(name = "os_type")
    private String typeOS;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at")
    private Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_at")
    private Date updatedAt;
}
