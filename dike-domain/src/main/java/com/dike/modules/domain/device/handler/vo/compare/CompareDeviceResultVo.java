package com.dike.modules.domain.device.handler.vo.compare;

import lombok.Builder;

import java.util.List;


@Builder
public class CompareDeviceResultVo {

    public List<DeviceInfoVo> result;
}
