package com.dike.modules.domain.device.mobile;

import com.dike.modules.domain.device.handler.condition.MobileDeviceInfoCondition;
import com.dike.modules.domain.device.handler.vo.compare.mobile.MobileAttributeDataVo;
import com.dike.modules.domain.device.handler.vo.compare.mobile.MobileDeviceInfoVo;
import com.dike.modules.domain.enums.device.BrandCode;

import java.util.List;

public interface MobileRepositoryCustom {

    List<String> findModelSeriesByBrandCodeAndSearchWord(BrandCode brandCode, String searchWord);

    List<MobileAttributeDataVo> getMobileAttributeDataVos(BrandCode brandCode, String modelSeriesName);

    MobileDeviceInfoVo getMobileDeviceInfoVo(MobileDeviceInfoCondition condition);
}
