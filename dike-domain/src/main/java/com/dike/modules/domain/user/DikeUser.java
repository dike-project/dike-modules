package com.dike.modules.domain.user;

import com.dike.modules.domain.auth.user.CustomUserDetails;
import com.dike.modules.domain.enums.user.GenderType;
import com.dike.modules.domain.enums.user.UserRole;
import com.dike.modules.domain.listener.*;
import lombok.*;


import javax.persistence.*;
import java.util.Date;

@Data
@Builder
@Entity
@Table(name = "dike_user", schema = "public")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@EntityListeners({CreatedAtListener.class, UpdatedAtListener.class, SoftDeleteListener.class})
public class DikeUser implements SystemMetaData, SoftDelete {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dike_user_seq")
    @SequenceGenerator(name = "dike_user_seq", sequenceName = "dike_user_seq", allocationSize = 1)
    @Column(name = "dike_user_id")
    private Long dikeUserId;

    @Column(name = "login_id")
    private String loginId;

    @Column(name = "password")
    private String password;

    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    private UserRole role;

    @Column(name = "nickname")
    private String nickname;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "age")
    private Integer age;

    @Column(name = "service_policy")
    private Boolean servicePolicy;

    @Column(name = "privacy_policy")
    private Boolean privacyPolicy;

    @Column(name = "marketing_policy")
    private Boolean marketingPolicy;

    @Enumerated(EnumType.STRING)
    @Column(name = "gender")
    private GenderType gender;

    @Column(name = "deleted")
    private Boolean deleted;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "deleted_at")
    private Date deletedAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at")
    private Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_at")
    private Date updatedAt;

    public static CustomUserDetails from(DikeUser user) {
        return CustomUserDetails.builder()
            .loginId(user.getLoginId())
            .password(user.getPassword())
            .role(user.getRole().getName())
            .build();
    }
}
