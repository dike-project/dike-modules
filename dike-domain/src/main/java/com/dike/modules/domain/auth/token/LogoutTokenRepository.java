package com.dike.modules.domain.auth.token;

import org.springframework.data.repository.CrudRepository;

public interface LogoutTokenRepository extends CrudRepository<LogoutToken, String> {
}
