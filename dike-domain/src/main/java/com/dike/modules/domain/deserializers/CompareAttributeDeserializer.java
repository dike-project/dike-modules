package com.dike.modules.domain.deserializers;

import com.dike.modules.domain.device.common.dto.compare.CompareAttributeDto;
import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;

public class CompareAttributeDeserializer extends JsonDeserializer<CompareAttributeDto> {
    @Override
    public CompareAttributeDto deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JacksonException {
        ObjectCodec codec = p.getCodec();
        JsonNode tree = codec.readTree(p);
        return null;
    }
}
