package com.dike.modules.domain.device.handler;

import com.dike.modules.domain.device.common.dto.SearchDeviceResponseDto;
import com.dike.modules.domain.device.common.dto.compare.CompareDeviceListDto;
import com.dike.modules.domain.device.handler.condition.MobileDeviceInfoCondition;
import com.dike.modules.domain.device.handler.vo.compare.DeviceCompareAttributeVo;
import com.dike.modules.domain.device.handler.vo.compare.DeviceInfoVo;
import com.dike.modules.domain.device.handler.vo.compare.mobile.MobileCompareAttributeVo;
import com.dike.modules.domain.device.mobile.MobileRepository;
import com.dike.modules.domain.enums.device.BrandCode;
import com.dike.modules.domain.enums.device.DeviceCategoryCode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static com.dike.modules.domain.enums.device.DeviceCategoryCode.MOBILE;


@Component
@RequiredArgsConstructor
public class MobileDeviceCategoryHandler implements DeviceCategoryHandler {

    private final MobileRepository mobileRepository;

    @Override
    public boolean isSupport(DeviceCategoryCode deviceCategoryCode) {
        return deviceCategoryCode.equals(MOBILE);
    }

    @Override
    public List<DeviceInfoVo> getDeviceInfoVos(CompareDeviceListDto compareDeviceListDto) {
        return compareDeviceListDto.getCompareDevices().stream()
                .map(compareDeviceDto -> mobileRepository.getMobileDeviceInfoVo(MobileDeviceInfoCondition.from(compareDeviceDto)))
                .collect(Collectors.toList());
    }

    @Override
    public DeviceCompareAttributeVo getDeviceAttributeVo(BrandCode brandCode, String modelSeriesName) {
        Set<Integer> storageCapacities = new HashSet<>();

        mobileRepository.getMobileAttributeDataVos(brandCode, modelSeriesName).forEach(
                vo -> {
                    if (Objects.nonNull(vo.getStorageCapacityGB())) {
                        storageCapacities.add(vo.getStorageCapacityGB());
                    }
                }
        );

        return MobileCompareAttributeVo.builder()
                .storageCapacities(storageCapacities)
                .build();
    }

    @Override
    public List<SearchDeviceResponseDto> searchDeviceBy(BrandCode brandCode, String targetModelName) {
        return mobileRepository.findAllByBrandCodeAndModelNameContaining(brandCode, targetModelName).stream()
                .map(e -> SearchDeviceResponseDto.builder()
                        .deviceId(e.getMobileId())
                        .brandCode(e.getBrandCode())
                        .deviceCategoryCode(MOBILE)
                        .deviceModelName(e.getSeries())
                        .build()
                )
                .collect(Collectors.toList());
    }

    @Override
    public List<String> searchDeviceModelSeriesBy(BrandCode brandCode, String searchWord) {
        return mobileRepository.findModelSeriesByBrandCodeAndSearchWord(brandCode, searchWord);
    }

    @Override
    public boolean checkDeviceExist(Long deviceId, BrandCode brandCode) {
        return mobileRepository.existsByMobileIdAndBrandCode(deviceId, brandCode);
    }

    @Override
    public boolean checkModelSeriesExist(BrandCode brandCode, String modelSeriesName) {
        return mobileRepository.existsByBrandCodeAndSeries(brandCode, modelSeriesName);
    }
}
