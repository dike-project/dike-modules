package com.dike.modules.domain.cms;

import com.dike.modules.domain.cms.BrandDevice.*;
import com.dike.modules.domain.cms.policy.Policy;
import com.dike.modules.domain.cms.policy.PolicyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class       : CmsService
 * Author      : 조 준 희
 * Description : Class Description
 * History     : [2022-07-22] - 조 준희 - Class Create
 */
@Service
public class CmsService {

    private BrandDeviceMappingRepository brandDeviceMappingRepository;
    private DeviceCategoryRepository deviceCategoryRepository;
    private PolicyRepository policyRepository;

    @Autowired
    public CmsService(BrandDeviceMappingRepository brandDeviceMappingRepository,
                      DeviceCategoryRepository deviceCategoryRepository,
                        PolicyRepository policyRepository ) {
        this.brandDeviceMappingRepository = brandDeviceMappingRepository;
        this.deviceCategoryRepository = deviceCategoryRepository;
        this.policyRepository = policyRepository;
    }

    public List<Policy> findAllPolicy(){

        List<Policy> result = policyRepository.findAll();
        return result;
    }


    public List<DeviceCategory> findAllDeviceCategories(){

        List<DeviceCategory> result = deviceCategoryRepository.findAll();

        result = result.stream().sorted((e1, e2) -> e1.getOrderIdx().compareTo(e2.getOrderIdx()))
                .collect(Collectors.toList());

        return result;

    }

    public List<BrandResponse> findAllBrandofDeviceCategory(Long deviceCategoryId){
        DeviceCategory searchEntity = DeviceCategory.builder().deviceCategoryId(deviceCategoryId).build();

        List<BrandDeviceMapping> a = brandDeviceMappingRepository.findAllByDeviceCategoryEntity(searchEntity);


        List<BrandResponse> brands = a.stream()
                .sorted(Comparator.comparing(BrandDeviceMapping::getOrderIdx))
                .map(e -> e.getBrandEntity().toResponse(e.getBrandEntity()))
                .collect(Collectors.toList());

        return brands;
    }


}
