package com.dike.modules.domain.common.auth;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Component
public class AuthenticationEntryPointHandler implements AuthenticationEntryPoint {

    private ObjectMapper om ;

    @Autowired
    public AuthenticationEntryPointHandler(ObjectMapper om) {
        this.om = om;
    }


    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setStatus(401);

        ServletOutputStream out = response.getOutputStream();
        om.writeValue(out,new CustomAuthenticationException().getResponseJsonObject());
        out.flush();
    }
}
