package com.dike.modules.domain.cms.policy;

import com.dike.modules.domain.cms.BrandDevice.BrandResponse;
import com.dike.modules.domain.listener.CreatedAtListener;
import com.dike.modules.domain.listener.UpdatedAtListener;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * Class       : Brand
 * Author      : 조 준 희
 * Description : Class Description
 * History     : [2022-07-22] - 조 준희 - Class Create
 */
@Entity
@Table(name = "policy", schema = "public")
@EntityListeners({CreatedAtListener.class, UpdatedAtListener.class})
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Policy {

    @Id
    @Column(name = "policy_id")
    private String policyId;

    @Column(name = "policy_name")
    private String policyName;

    @Column(name = "isOptional")
    private Boolean isOptional;

    @Column(name = "link")
    private String link;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at")
    private Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_at")
    private Date updatedAt;

    public PolicyResponse toResponse(Policy entity){
        return PolicyResponse.builder().policyId(entity.policyId)
                .isOptional(entity.isOptional)
                .policyName(entity.policyName)
                .link(entity.link)
                .build();
    }

}
