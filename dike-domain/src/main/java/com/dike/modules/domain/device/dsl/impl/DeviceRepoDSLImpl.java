package com.dike.modules.domain.device.dsl.impl;

import com.dike.modules.domain.cms.BrandDevice.Brand;
import com.dike.modules.domain.common.response.ApiStatusCode;
import com.dike.modules.domain.device.QDikeUserDevice;
import com.dike.modules.domain.device.dsl.DeviceRepoDSL;
import com.dike.modules.domain.device.earphone.QEarphone;
import com.dike.modules.domain.device.laptop.QLaptop;
import com.dike.modules.domain.device.mobile.QMobile;
import com.dike.modules.domain.device.tablet.QTablet;
import com.dike.modules.domain.device.watch.QWatch;
import com.dike.modules.domain.enums.device.BrandCode;
import com.dike.modules.domain.exception.DikeServiceException;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

/**
 * Class       : DeviceRepoDSLImpl
 * Author      : 조 준 희
 * Description : Class Description
 * History     : [2022-09-22] - 조 준희 - Class Create
 */
@Repository
@Log4j2
public class DeviceRepoDSLImpl implements DeviceRepoDSL {
    private final static Logger logger = LogManager.getLogger(DeviceRepoDSLImpl.class);
    private final JPAQueryFactory jpaQueryFactory;

    @Autowired
    public DeviceRepoDSLImpl(JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }


    /**
     * Description : 사용자 디바이스 등록
     * Name        : SetUserDevice
     * Author      : 조 준 희
     * History     : [2022-09-23] - 조 준 희 - Create
     */
    @Override
    public void setUserDevice(String userId, BrandCode brandCode, String deviceType, String series, String reviewKeywords, int buyYear) {

        // 등록하려는 디바이스 유효성 체크

        Integer isDeviceExists = null;

        if(deviceType.equals("watch")){
            isDeviceExists =  jpaQueryFactory.selectOne()
                                        .from(QWatch.watch)
                                        .where(QWatch.watch.brandCode.eq(brandCode)
                                        .and(QWatch.watch.series.eq(series)))
                                        .limit(1).fetchOne();
        }

//        if(deviceType.equals("laptop")){
//            isDeviceExists =  jpaQueryFactory.selectOne()
//                    .from(QLaptop.laptop)
//                    .where(QLaptop.laptop.brandCode.eq(brandCode)
//                            .and(QLaptop.laptop.laptopId.eq(series)))
//                    .limit(1).fetchOne();
//        }

//        if(deviceType.equals("mobile")){
//            isDeviceExists =  jpaQueryFactory.selectOne()
//                    .from(QMobile.mobile)
//                    .where(QMobile.mobile.brandCode.eq(brandCode)
//                            .and(QMobile.mobile.mobileId.eq(series)))
//                    .limit(1).fetchOne();
//        }
//
//        if(deviceType.equals("earphone")){
//            isDeviceExists =  jpaQueryFactory.selectOne()
//                    .from(QEarphone.earphone)
//                    .where(QEarphone.earphone.brandCode.eq(brandCode)
//                            .and(QEarphone.earphone.earphoneId.eq(deviceId)))
//                    .limit(1).fetchOne();
//        }
//
//        if(deviceType.equals("tablet")){
//            isDeviceExists =  jpaQueryFactory.selectOne()
//                    .from(QTablet.tablet)
//                    .where(QTablet.tablet.brandCode.eq(brandCode)
//                            .and(QTablet.tablet.tabletId.eq(deviceId)))
//                    .limit(1).fetchOne();
//        }

        if(isDeviceExists == null || isDeviceExists.intValue() < 1)
            throw new DikeServiceException(ApiStatusCode.SYSTEM_ERROR,"Not exists device model. ");

          long insertCnt = jpaQueryFactory.insert(QDikeUserDevice.dikeUserDevice)
                                          .columns( QDikeUserDevice.dikeUserDevice.loginId
                                                  ,QDikeUserDevice.dikeUserDevice.deviceCategoryCode
                                                  ,QDikeUserDevice.dikeUserDevice.brandCode
                                                  ,QDikeUserDevice.dikeUserDevice.series
                                                  ,QDikeUserDevice.dikeUserDevice.buyYear
                                                  ,QDikeUserDevice.dikeUserDevice.reviewKeywords
                                          ,QDikeUserDevice.dikeUserDevice.updatedAt
                                          , QDikeUserDevice.dikeUserDevice.createdAt)
                                          .values(userId, deviceType,brandCode, series, buyYear, reviewKeywords, LocalDateTime.now(), LocalDateTime.now()).execute();

          if(log.isDebugEnabled())
              log.debug("Set User Device   Result Cnt : {}",insertCnt);

          return ;
    }
}
