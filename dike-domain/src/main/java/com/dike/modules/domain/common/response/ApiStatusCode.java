package com.dike.modules.domain.common.response;
import lombok.Getter;
import org.springframework.http.HttpStatus;

/** Class       : ApiStatusCode (Enum)
 *  Author      : 조 준 희
 *  Description : 모든 API에서 사용될 API 처리 상태 코드관리 Enum Class
 *                  - COMMON CODE - 공용 도메인 코드로 모든 도메인에서 사용될 공통 코드 분류
 *                  - ** 추가적인 도메인 코드가 생길 경우 //도메인명 CODE 와 같이 주석 후 코드 정리할 것.
 *                  - 표준 HTTP Status Code는 피할 것.
 *                  - 표준 HTTP Status : 100,200,201,204,206
 *                                      301,302,303,304,307,308
 *                                      401,403,404,405,406,407,409,410,412,416,418,425,451
 *                                      500,501,502,503,504
 *  History     : [2022-03-11] - TEMP
 */
@Getter
public enum ApiStatusCode {

    //COMMON CODE
    NONE (0, null, null, null)

    ,OK (200, null, null, false)
    ,NO_CONTENT (204, "No Content", "콘텐츠 없음.",false)

    ,NOT_MATCHED_CERTIFICATION_NUMBER(310, "SMS authentication failed.", "SMS 인증 실패.", true)
    ,NOT_EXIST_CERTIFICATION_NUMBER(311, "No SMS transmission history.", "해당 수신자 번호로 전송된 인증 문자 정보가 존재하지 않습니다.", false)
    ,ALREADY_REGISTERED_USER(312, "You are already a registered user.", "이미 가입된 사용자입니다.", false)


    ,PARAMETER_CHECK_FAILED (400,"Bad Request","문법상 또는 파라미터 오류가 있어서 서버가 요청사항을 처리하지 못함.", true)
    ,INVALID_PARAMETER (400,"Bad Request","유효하지 않는 파라미터로 서버가 요청을 처리할 수 없음", true)
    ,UNAUTHORIZED (401, "UnAuthorized", "Unauthorized, 사용자 인증 실패.", true)
    ,FORBIDDEN (403, "Forbidden", "Forbidden, 사용권한 없음.", true)
    ,TOKEN_NOT_EXIST(401, "Bad Request", "필수 토큰 정보가 누락되었습니다.", true)


    ,USERNAME_NOT_FOUND(444, "아이디가 일치하지 않습니다.","아이디가 일치하지 않습니다.", true)
    ,WRONG_PASSWORD(444, "비밀번호가 일치하지 않습니다.","비밀번호가 일치하지 않습니다.", true)

    ,NOT_MATCHED_REFRESH_TOKEN(444, "리프레시 토큰 정보가 일치하지 않습니다.", "리프레시 토큰 정보가 일치하지 않습니다.", true)
    ,NOT_EXIST_REFRESH_TOKEN(444, "해당 아이디에 대한 리프레시 토큰이 유효하지 않습니다.", "해당 아이디에 대한 리프레시 토큰이 유효하지 않습니다.", true)

    ,LOGOUT_USER(444, "이미 로그아웃된 회원입니다.", "이미 로그아웃된 회원입니다.", true)



    ,SYSTEM_ERROR(500,"System Error.", "시스템 오류.", true)
    ;

    //Enum 필드
    private int code;
    private String type;
    private String message;
    private Boolean isErrorType;     // API 결과 타입이 Error 타입인지.

    //Enum 생성자
    ApiStatusCode(int code, String type , String message, Boolean isErrorType) {
        this.code = code;
        this.type = type;
        this.message = message;
        this.isErrorType = isErrorType;
    }
}
