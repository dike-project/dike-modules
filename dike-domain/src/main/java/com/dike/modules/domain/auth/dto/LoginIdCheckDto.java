package com.dike.modules.domain.auth.dto;



import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;


@Data
@Builder
@AllArgsConstructor
public class LoginIdCheckDto {

    private boolean isDuplicated;
}
