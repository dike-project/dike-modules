package com.dike.modules.domain.enums.device;

import com.dike.modules.domain.enums.DikeEnum;

public enum DeviceCategoryCode implements DikeEnum {
    LAPTOP("노트북"),
    MOBILE("휴대폰"),
    TABLET("테블릿"),
    WATCH("스마트워치"),
    EARPHONE("이어폰")
    ;

    private String description;

    DeviceCategoryCode(String description) {
        this.description = description;
    }

    @Override
    public String getName() {
        return this.name();
    }

    @Override
    public String getDescription() {
        return this.description;
    }
}
