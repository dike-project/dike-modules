package com.dike.modules.domain.auth.dto;


import com.dike.modules.domain.exception.DikeServiceException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

import static com.dike.modules.domain.common.response.ApiStatusCode.PARAMETER_CHECK_FAILED;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PasswordRecoverDto {

    @NotBlank(message = "새 비밀번호는 필수값입니다.")
    private String newPassword;
}
