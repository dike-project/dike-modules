package com.dike.modules.domain.listener;

import java.io.Serializable;
import java.util.Date;

public interface SystemMetaData extends Serializable {

    void setCreatedAt(final Date date);

    void setUpdatedAt(final Date date);
}
