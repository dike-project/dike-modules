package com.dike.modules.domain.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DikeUserRepository extends JpaRepository<DikeUser, Long> {

    Optional<DikeUser> findByLoginId(String loginId);
    boolean existsByPhoneNumber(String phoneNumber);
    Optional<DikeUser> findFirstByNicknameIsIn(List<String> nickname);
    void deleteByLoginId(String loginId);

    Boolean existsDikeUserByLoginIdAndPhoneNumber(String loginId, String phoneNumber);
}
