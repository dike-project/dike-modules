package com.dike.modules.domain.cms.BrandDevice;


import com.dike.modules.domain.enums.device.DeviceCategoryCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@AllArgsConstructor
@Builder
public class DeviceCategoryResponse {
    private Long deviceCategoryId;
    private DeviceCategoryCode deviceCategoryCode;
    private String deviceCategoryName;
    private String deviceCategoryIcon;
}
