package com.dike.modules.domain.device.handler;

import com.dike.modules.domain.device.common.dto.SearchDeviceResponseDto;
import com.dike.modules.domain.device.common.dto.compare.CompareDeviceListDto;
import com.dike.modules.domain.device.handler.vo.compare.DeviceCompareAttributeVo;
import com.dike.modules.domain.device.handler.vo.compare.DeviceInfoVo;
import com.dike.modules.domain.enums.device.BrandCode;
import com.dike.modules.domain.enums.device.DeviceCategoryCode;

import java.util.List;

public interface DeviceCategoryHandler {

    boolean isSupport(DeviceCategoryCode deviceCategoryCode);

    List<DeviceInfoVo> getDeviceInfoVos(CompareDeviceListDto compareDeviceListDto);

    DeviceCompareAttributeVo getDeviceAttributeVo(BrandCode brandCode, String modelSeriesName);

    List<SearchDeviceResponseDto> searchDeviceBy(BrandCode brandCode, String targetModelName);

    List<String> searchDeviceModelSeriesBy(BrandCode brandCode, String searchWord);

    boolean checkDeviceExist(Long deviceId, BrandCode brandCode);

    boolean checkModelSeriesExist(BrandCode brandCode, String modelSeriesName);

}
