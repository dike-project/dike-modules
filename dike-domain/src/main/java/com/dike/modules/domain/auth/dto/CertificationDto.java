package com.dike.modules.domain.auth.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CertificationDto {

    @NotBlank(message = "가입자 휴대폰 번호는 필수값입니다.")
    private String receiver;

    @NotBlank(message = "입력한 인증번호는 필수값입니다.")
    private String targetNumber;
}
