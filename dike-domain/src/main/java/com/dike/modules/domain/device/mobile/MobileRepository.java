package com.dike.modules.domain.device.mobile;

import com.dike.modules.domain.enums.device.BrandCode;
import io.lettuce.core.dynamic.annotation.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MobileRepository extends JpaRepository<Mobile, Long>, MobileRepositoryCustom {
    boolean existsByMobileIdAndBrandCode(Long mobileId, BrandCode brandCode);

    boolean existsByBrandCodeAndSeries(BrandCode brandCode, String series);

    @Query(value = "SELECT m FROM Mobile m WHERE m.brandCode = :#{#brandCode} AND REPLACE(m.modelCode,' ','') LIKE CONCAT('%', :modelName,'%')")
    List<Mobile> findAllByBrandCodeAndModelNameContaining(@Param("brandCode") BrandCode brandCode, String modelName);
}
