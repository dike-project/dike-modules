package com.dike.modules.domain.configuration;

import com.dike.modules.domain.enums.device.BrandCodeRequestConverter;
import com.dike.modules.domain.enums.device.DeviceCategoryCodeConverter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.time.LocalDateTime;

/**
 * Class       : BeanConfig
 * Author      : 조 준 희
 * Description : Class Description
 * History     : [2022-07-04] - 조 준희 - Class Create
 */
@Configuration
public class BeanConfig implements WebMvcConfigurer {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {

        registry.addConverter(new BrandCodeRequestConverter());
        registry.addConverter(new DeviceCategoryCodeConverter());
    }

    @Bean
    public ObjectMapper objectMapper() {
        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addSerializer(LocalDateTime.class, new CustomLocalDateTimeSerializer());

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);   // Java 객체를 JSON으로 Serialize할 때 null값은 제외
        objectMapper.registerModule(simpleModule);
        return objectMapper;
    }
}
