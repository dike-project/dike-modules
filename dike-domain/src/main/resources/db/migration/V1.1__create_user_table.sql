drop table if exists dike_user;

create table dike_user
(

    dike_user_id    bigint                              not null,
    login_id        varchar(50)                         not null,
    gender          varchar(20),
    password        varchar(200)                        not null,
    age             integer,
    phone_number    varchar(50)                         not null,
    role            varchar(20)                         not null,
    nickname        varchar(50)                         not null,
    service_policy boolean                             ,
    privacy_policy boolean                             ,
    marketing_policy boolean                           ,
    deleted         boolean,
    deleted_at      timestamp,
    created_at      timestamp default current_timestamp not null,
    updated_at      timestamp default current_timestamp not null,
    primary key (dike_user_id)
);

alter table dike_user
    add constraint dike_user_uk01 unique (login_id);
alter table dike_user
    add constraint dike_user_uk02 unique (phone_number);
alter table dike_user
    add constraint dike_user_uk03 unique (nickname);

drop sequence if exists dike_user_seq;
create sequence dike_user_seq increment by 1 start 1 no cycle;
