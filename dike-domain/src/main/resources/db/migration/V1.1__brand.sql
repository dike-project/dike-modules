drop table if exists brand_device_mapping;
drop table if exists brand;

create table brand
(

    brand_id       bigint                              not null,
    brand_name     varchar(50)                         not null,
    brand_code     varchar(30)                         not null,
    brand_name_eng varchar(20)                         not null,
    brand_icon     varchar(200)                        not null,
    created_at     timestamp default current_timestamp not null,
    updated_at     timestamp default current_timestamp not null,
    primary key (brand_id)
);

alter table brand
    add constraint brand_uk01 unique (brand_code);

drop sequence if exists brand_seq;
create sequence brand_seq increment by 1 start 1 no cycle;


insert into brand (brand_id,
                   brand_name,
                   brand_code,
                   brand_name_eng,
                   brand_icon,
                   created_at,
                   updated_at)
values (1, '애플',
        'APPLE',
        'Apple',
        'https://res.cloudinary.com/ddmwhoqt9/image/upload/v1658476969/dike/brand/brand_Apple_tnjl1h.png',
        now(),
        now()),

       (2, '삼성전자',
        'SAMSUNG',
        'Samsung',
        'https://res.cloudinary.com/ddmwhoqt9/image/upload/v1658476969/dike/brand/brand_Samsung_uqirjl.png',
        now(),
        now()),

       (3,
        'LG전자',
        'LG',
        'LG',
        'https://res.cloudinary.com/ddmwhoqt9/image/upload/v1658476969/dike/brand/brand_LG_n5ij4o.png',
        now(),
        now()),

       (4,
        '보스',
        'BOSE',
        'Bose',
        'https://res.cloudinary.com/ddmwhoqt9/image/upload/v1658476969/dike/brand/brand_Bose_mkkke9.png',
        now(),
        now()),

       (5,
        '델',
        'DELL',
        'Dell',
        'https://res.cloudinary.com/ddmwhoqt9/image/upload/v1658476969/dike/brand/brand_Apple_tnjl1h.png',
        now(),
        now()),

       (6,
        '브리츠',
        'BRITZ',
        'Britz',
        'https://res.cloudinary.com/ddmwhoqt9/image/upload/v1658476969/dike/brand/brand_Britz_fhekia.png',
        now(),
        now()),

       (7,
        '레노버',
        'LENOVO',
        'Lenovo',
        'https://res.cloudinary.com/ddmwhoqt9/image/upload/v1658476969/dike/brand/brand_Lenovo_u7n8mc.png',
        now(),
        now()),

       (8,
        '샥즈',
        'SHOKZ',
        'SHOKZ',
        'https://res.cloudinary.com/ddmwhoqt9/image/upload/v1658476969/dike/brand/brand_Shokz_s7olyg.png',
        now(),
        now()),

       (9, '샤오미',
        'XIAOMI',
        'Xiaomi',
        'https://res.cloudinary.com/ddmwhoqt9/image/upload/v1658476970/dike/brand/brand_mii_f8n4zi.png',
        now(),
        now()),

       (10, '소니',
        'SONY',
        'SONY',
        'https://res.cloudinary.com/ddmwhoqt9/image/upload/v1658476970/dike/brand/brand_Sony_pdsur7.png',
        now(),
        now());



drop table if exists device_category;

create table device_category
(

    device_category_id   bigint                              not null,
    device_category_name varchar(50)                         not null,
    device_category_icon varchar(200),
    device_category_code varchar(20)                         not null,
    order_idx            int       default 999,
    created_at           timestamp default current_timestamp not null,
    updated_at           timestamp default current_timestamp not null,
    primary key (device_category_id)
);

alter table device_category
    add constraint device_category_uk01 unique (device_category_code);

drop sequence if exists device_category_seq;
create sequence device_category_seq increment by 1 start 1 no cycle;

insert into device_category (device_category_id,
                             device_category_name,
                             device_category_icon,
                             device_category_code,
                             order_idx,
                             created_at,
                             updated_at)
values (1,
        '태블릿',
        'https://res.cloudinary.com/ddmwhoqt9/image/upload/v1658476970/dike/category/category_tablet_etwiog.png',
        'TABLET',
        1,
        now(),
        now()),

       (2,
        '핸드폰',
        'https://res.cloudinary.com/ddmwhoqt9/image/upload/v1658476970/dike/category/category_phone_e5a5qm.png',
        'MOBILE',
        2,
        now(),
        now()),

       (3,
        '스마트워치',
        'https://res.cloudinary.com/ddmwhoqt9/image/upload/v1658476970/dike/category/category_watch_bxauo0.png',
        'WATCH',
        3,
        now(),
        now()),

       (4,
        '노트북',
        'https://res.cloudinary.com/ddmwhoqt9/image/upload/v1658476970/dike/category/category_notebook_ozemnf.png',
        'LAPTOP',
        4,
        now(),
        now()),

       (5,
        '이어폰',
        'https://res.cloudinary.com/ddmwhoqt9/image/upload/v1658476970/dike/category/category_earphone_taolfu.png',
        'EARPHONE',
        5,
        now(),
        now())
;



create table brand_device_mapping
(
    seq                  bigint                              not null,
    device_category_id   bigint                              not null,
    device_category_code varchar(20)                         not null,
    brand_id             bigint                              not null,
    brand_code           varchar(20)                         not null,
    order_idx            int       default 999,
    created_at           timestamp default current_timestamp not null,
    updated_at           timestamp default current_timestamp not null,
    foreign key (brand_id) references brand,
    foreign key (device_category_id) references device_category,
    unique (device_category_id, brand_id)
);
drop sequence if exists brand_device_mapping_seq;
create sequence brand_device_mapping_seq increment by 1 start 1 no cycle;

delete
from brand_device_mapping;

-- 태블릿
insert into brand_device_mapping(seq,
                                 device_category_id,
                                 device_category_code,
                                 brand_id,
                                 brand_code,
                                 order_idx,
                                 created_at,
                                 updated_at)
values (1, 1, 'TABLET', 1, 'APPLE', 1, now(), now()),
       (2, 1, 'TABLET', 2, 'SAMSUNG', 2, now(), now()),
       (3, 1, 'TABLET', 7, 'LENOVO', 3, now(), now());


-- 휴대폰
insert into brand_device_mapping(seq,
                                 device_category_id,
                                 device_category_code,
                                 brand_id,
                                 brand_code,
                                 order_idx,
                                 created_at,
                                 updated_at)
values (4, 2, 'MOBILE', 2, 'SAMSUNG', 1, now(), now()),
       (5, 2, 'MOBILE', 1, 'APPLE', 2, now(), now()),
       (6, 2, 'MOBILE', 3, 'LG', 3, now(), now());


-- 워치
insert into brand_device_mapping(seq,
                                 device_category_id,
                                 device_category_code,
                                 brand_id,
                                 brand_code,
                                 order_idx,
                                 created_at,
                                 updated_at)
values (7, 3, 'WATCH', 2, 'SAMSUNG', 1, now(), now()),
       (8, 3, 'WATCH', 1, 'APPLE', 2, now(), now()),
       (9, 3, 'WATCH', 9, 'XIAOMI', 3, now(), now());

-- 노트북
insert into brand_device_mapping(seq,
                                 device_category_id,
                                 device_category_code,
                                 brand_id,
                                 brand_code,
                                 order_idx,
                                 created_at,
                                 updated_at)
values (10, 4, 'LAPTOP', 2, 'SAMSUNG', 1, now(), now()),
       (11, 4, 'LAPTOP', 1, 'APPLE', 2, now(), now()),
       (12, 4, 'LAPTOP', 3, 'LG', 3, now(), now()),
       (13, 4, 'LAPTOP', 7, 'LENOVO', 4, now(), now()),
       (14, 4, 'LAPTOP', 5, 'DELL', 5, now(), now());

-- 이어폰
insert into brand_device_mapping(seq,
                                 device_category_id,
                                 device_category_code,
                                 brand_id,
                                 brand_code,
                                 order_idx,
                                 created_at,
                                 updated_at)
values (15, 5, 'EARPHONE', 2, 'SAMSUNG', 1, now(), now()),
       (16, 5, 'EARPHONE', 1, 'APPLE', 2, now(), now()),
       (17, 5, 'EARPHONE', 8, 'SHOKZ', 3, now(), now()),
       (18, 5, 'EARPHONE', 10, 'SONY', 4, now(), now()),
       (19, 5, 'EARPHONE', 6, 'BRITZ', 5, now(), now()),
       (20, 5, 'EARPHONE', 4, 'BOSE', 6, now(), now());



