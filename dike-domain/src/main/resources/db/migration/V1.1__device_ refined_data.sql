
drop table if exists dike_user_device;

create table dike_user_device
(

    seq             bigint                              not null,
    login_id        varchar(50)                         not null,
    device_category_code     varchar(20)                         not null,
    brand_code      varchar(30)                         not null,
    device_id       bigint                              not null,
    buy_year        integer                             not null,
    review_keywords varchar(100)                        not null,
    grade           integer                             not null,
    review_content  varchar(1000)                       not null,
    created_at      timestamp default current_timestamp not null,
    updated_at      timestamp default current_timestamp not null,
    primary key (seq)
);

alter table dike_user_device
    add constraint dike_user_device_uk01 unique (login_id, device_category_code, brand_code, device_id) ;

drop sequence if exists dike_user_device_seq;
create sequence dike_user_device_seq increment by 1 start 1 no cycle;


drop table if exists review_keywords;
create table review_keywords
(
    seq                     bigint                         not null,
    device_category_code    varchar(20)                         not null,
    order_idx               int                                 default 999,
    keyword                 varchar(100)                    not null,
    created_at              timestamp default current_timestamp not null,
    updated_at              timestamp default current_timestamp not null,
    primary key (seq)
);

drop sequence if exists review_keywords_seq;
create sequence review_keywords_seq increment by 1 start 1 no cycle;



insert into review_keywords (seq, device_category_code,order_idx,keyword ,created_at, updated_at)
values( 1, 'WATCH', 1, '가벼워요' , now(), now() ),
      ( 2, 'WATCH', 2, '배터리가 오래가요' , now(), now() ),
      ( 3, 'WATCH', 3, '예뻐요' , now(), now() ),
      ( 4, 'WATCH', 4, '화면크기가 적당해요' , now(), now() ),
      ( 5, 'WATCH', 5, '가성비 좋아요' , now(), now() ),
      ( 6, 'WATCH', 6, '조작이 쉬워요' , now(), now() ),
      ( 7, 'WATCH', 7, '생활기스/방수에 강해요' , now(), now() ),
      ( 8, 'WATCH', 8, '착용감이 좋아요' , now(), now() ),
      ( 9, 'WATCH', 9, '건강관리에 유용해요' , now(), now() ),
      ( 10, 'WATCH', 10, '다양한 앱을 지원해요' , now(), now() ),
      ( 11, 'WATCH', 999, '기기호환이 원활해요' , now(), now() )

