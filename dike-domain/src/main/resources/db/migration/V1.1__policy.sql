drop table if exists policy;

create table policy
(

    policy_id       varchar(20)                              not null,
    policy_name     varchar(50)                         not null,
    isOptional      boolean,
    link            varchar(200)                        not null,
    created_at                  timestamp default current_timestamp not null,
    updated_at                  timestamp default current_timestamp not null,
    primary key (policy_id)
);

insert into policy values('service_policy', '이용약관 동의', false, '',now(), now());
insert into policy values('privacy_policy', '개인정보 처리방침 동의', false, '',now(), now());
insert into policy values('marketing_policy', '혜택 및 마케팅 정보 수신 동의', true, '',now(), now());
