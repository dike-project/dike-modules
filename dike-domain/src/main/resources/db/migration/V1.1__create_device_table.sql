-- 노트북
drop table if exists laptop;
create table laptop
(
    laptop_id           bigint                              not null,
    brand_code          varchar(30)                         not null,
    registered_at       date,
    screen_inch         double precision,
    screen_resolution   varchar(50),
    thickness_mm        double precision,
    weight_kg           double precision,
    cpu_manufacturer    varchar(30),
    cpu_type            varchar(50),
    cpu_number          varchar(50),
    core_type           varchar(50),
    ram_gb              double precision,
    ram_exchangeable    boolean,
    storage_capacity_gb integer,
    gpu_built_in        boolean,
    gpu_manufacturer    varchar(30),
    gpu_memory_gb       integer,
    hdmi_port           boolean,
    usb_port_number     integer,
    usb_type_c          boolean,
    usb_type_a          boolean,
    micro_sd_card       boolean,
    security            varchar(20),
    charging_type       varchar(50),
    purpose             varchar(100),
    model_code          varchar(100),
    model_name          varchar(200),
    image_path          varchar(500),
    original_price      integer,
    colors              varchar(100),
    created_at          timestamp default current_timestamp not null,
    updated_at          timestamp default current_timestamp not null,
    primary key (laptop_id)
);

drop sequence if exists laptop_seq;
create sequence laptop_seq increment by 1 start 1 no cycle;

-- 테블릿
drop table if exists tablet;
create table tablet
(
    tablet_id           bigint                              not null,
    brand_code          varchar(30)                         not null,
    series              varchar(30)                         not null,
    registered_at       date,
    model_code          varchar(100),
    screen_inch         double precision,
    screen_resolution   varchar(50),
    rear_camera         varchar(200),
    front_camera        varchar(100),
    charging_type       varchar(50),
    weight_g            double precision,
    thickness_mm        double precision,
    screen_width_mm     double precision,
    screen_height_mm    double precision,
    network_type        varchar(30),
    ram_gb              double precision,
    storage_capacity_gb double precision,
    model_name          varchar(100),
    original_price      integer,
    colors              varchar(100),
    pencil_support      varchar(30),
    image_path          varchar(500),
    battery_time_hour   double precision,
    created_at          timestamp default current_timestamp not null,
    updated_at          timestamp default current_timestamp not null,
    primary key (tablet_id)
);

drop sequence if exists tablet_seq;
create sequence tablet_seq increment by 1 start 1 no cycle;

-- 휴대폰
drop table if exists mobile;
create table mobile
(
    mobile_id           bigint                              not null,
    brand_code          varchar(30)                         not null,
    series              varchar(30)                         not null,
    registered_at       date,
    model_code          varchar(100),
    screen_inch         double precision,
    screen_resolution   varchar(50),
    camera_type         varchar(50),
    rear_camera         varchar(200),
    front_camera        varchar(100),
    water_dust_proof    varchar(20),
    charging_type       varchar(50),
    weight_g            double precision,
    thickness_mm        double precision,
    screen_width_mm     double precision,
    screen_height_mm    double precision,
    network_type        varchar(30),
    ram_gb              integer,
    storage_capacity_gb integer,
    security            varchar(20),
    image_path          varchar(500),
    original_price      integer,
    colors              varchar(100),
    created_at          timestamp default current_timestamp not null,
    updated_at          timestamp default current_timestamp not null,
    primary key (mobile_id)
);

drop sequence if exists mobile_seq;
create sequence mobile_seq increment by 1 start 1 no cycle;

-- 워치
drop table if exists watch;
create table watch
(
    watch_id             bigint                              not null,
    brand_code           varchar(30)                         not null,
    series               varchar(30)                         not null,
    registered_at        date,
    device_shape         varchar(20),
    screen_resolution    varchar(50),
    screen_mm            double precision,
    screen_inch          double precision,
    ram_gb               double precision,
    material_type        varchar(20),
    model_name           varchar(100),
    image_path           varchar(500),
    original_price       integer,
    colors               varchar(100),
    weight_g             double precision,
    exercise_support     varchar(300),
    health_support       varchar(300),
    battery_time_hour    integer,
    loss_prevention      boolean,
    wireless_charging    boolean,
    alarm_support        varchar(100),
    ai_voice_recognition boolean,
    water_dust_proof     varchar(20),
    network_type         varchar(30),
    gps                  boolean,
    nfc                  boolean,
    callable             boolean,
    created_at           timestamp default current_timestamp not null,
    updated_at           timestamp default current_timestamp not null,
    primary key (watch_id)
);

drop sequence if exists watch_seq;
create sequence watch_seq increment by 1 start 1 no cycle;

-- 이이폰
drop table if exists earphone;
create table earphone
(
    earphone_id            bigint                              not null,
    brand_code             varchar(30)                         not null,
    registered_at          date,
    purpose                varchar(100),
    unit_type              varchar(20),
    active_noise_canceling boolean,
    surrounding_listening  boolean,
    touch_button           boolean,
    hand_free_call         boolean,
    water_proof            varchar(20),
    play_time_hour         double precision,
    call_time_hour         double precision,
    case_charging_number   double precision,
    charging_type          varchar(50),
    weight_g               double precision,
    model_code             varchar(100),
    model_name             varchar(100),
    image_path             varchar(500),
    original_price         integer,
    colors                 varchar(100),
    created_at             timestamp default current_timestamp not null,
    updated_at             timestamp default current_timestamp not null,
    primary key (earphone_id)
);

drop sequence if exists earphone_seq;
create sequence earphone_seq increment by 1 start 1 no cycle;