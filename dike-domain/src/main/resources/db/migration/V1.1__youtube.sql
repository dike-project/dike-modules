drop table if exists youtube_recommend;

-- 유튜브 카테고리 관리.
create table youtube_recommend
(
    category_code       varchar(50)                         not null,
    category_name     varchar(50)                         not null,
    videos              varchar(200)                        not null,
    order_idx         int                                 not null,
    created_at        timestamp default current_timestamp not null,
    updated_at        timestamp default current_timestamp not null,
    primary key (category_code)
);

insert into youtube_recommend (category_code,
                              category_name,
                              videos,
                              order_idx,
                              created_at,
                              updated_at)
values ('iPhone_14_Series',
        '아이폰 14 시리즈',
        'VOEmkMqcxwU,Za3OW0TjdQw,i3ie6nY-AsE',
        1,
        now(),
        now()),
       ('iPhone_13_Series',
        '아이폰 13 시리즈',
        'VOEmkMqcxwU,Za3OW0TjdQw,i3ie6nY-AsE',
        2,
        now(),
        now());
