package com.dike.modules.api.configuration;

import com.dike.modules.domain.common.auth.AuthenticationEntryPointHandler;
import com.dike.modules.domain.common.auth.JwtAuthenticationFilter;
import com.dike.modules.domain.common.auth.JwtTokenProvider;
import com.dike.modules.domain.common.auth.WebAccessDeniedHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.CorsUtils;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;


@Configuration
@EnableWebSecurity
public class DikeSpringSecurityConfig extends WebSecurityConfigurerAdapter {

    private final AuthenticationEntryPointHandler authenticationEntryPoint;
    private final WebAccessDeniedHandler accessDeniedHandler;
    private final JwtTokenProvider jwtTokenProvider;
    private final AuthenticationManagerBuilder authenticationManagerBuilder;

    public DikeSpringSecurityConfig(AuthenticationEntryPointHandler authenticationEntryPoint
            , WebAccessDeniedHandler accessDeniedHandler
            , JwtTokenProvider jwtTokenProvider
            , AuthenticationManagerBuilder authenticationManagerBuilder) {
        this.authenticationEntryPoint = authenticationEntryPoint;
        this.accessDeniedHandler = accessDeniedHandler;
        this.jwtTokenProvider = jwtTokenProvider;
        this.authenticationManagerBuilder = authenticationManagerBuilder;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .httpBasic().disable()
                .csrf().disable()

                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                /**
                 * URI별 인가 정보 셋팅. - 조준희
                 */
                .requestMatchers(CorsUtils::isPreFlightRequest).permitAll()
                // user
                .antMatchers(HttpMethod.POST, "/user/join/**").permitAll() // 회원가입
                .antMatchers(HttpMethod.GET, "/user/check/duplication/**").permitAll() // 아이디 중복체크
                .antMatchers(HttpMethod.GET, "/user/send/certification/**").permitAll()  // 인증 문자 발송
                .antMatchers(HttpMethod.POST, "/user/login/**").permitAll() // 로그인.
                .antMatchers(HttpMethod.POST, "/user/logout/**").authenticated() // 로그아웃.
                .antMatchers(HttpMethod.POST, "/user/check/certification/**").permitAll() // 인증 문자 확인
                .antMatchers(HttpMethod.POST, "/user/check/password-certification/**").permitAll() // 비밀번호 변경 인증 문자 확인
                .antMatchers(HttpMethod.PUT, "/user/password/recover/**").permitAll() // 비밀번호 변경
                .antMatchers(HttpMethod.POST, "/user/reissue/**").authenticated() // 액세스 토큰 재발급


                .antMatchers(HttpMethod.GET, "/cms/devices/**").permitAll() // 기기 카테고리 조회.
                .antMatchers(HttpMethod.GET, "/cms/*/brand/**").permitAll() // 기기 카테고리별 브랜드 조회
                .antMatchers(HttpMethod.GET, "/cms/policy/**").permitAll() // 이용약관 조회
                .antMatchers(HttpMethod.GET, "/cms/youtube/recommended/**").permitAll() // 유튜브 조회


                .antMatchers(HttpMethod.POST, "/device/user/**").authenticated() //   사용자 기기 등록
                .antMatchers(HttpMethod.GET, "/device/*/*/*/**").permitAll() //  기기 검색
                .antMatchers(HttpMethod.GET, "/device/*/search/*/**").permitAll() //  기기 시리즈 검색
                .antMatchers(HttpMethod.GET, "/device/keywords/*/**").permitAll() //  기기 리뷰 키워드 조회
                .antMatchers(HttpMethod.GET, "/device/*/compare-attributes/**").permitAll() //  기기 리뷰 키워드 조회
                .antMatchers(HttpMethod.POST, "/device/*/compare").permitAll() //  기기 리뷰 키워드 조회

                .anyRequest().authenticated()
                .and()
                .cors().configurationSource(corsConfigurationSource())

                .and()
                .logout().disable()

                .exceptionHandling()
                .authenticationEntryPoint(authenticationEntryPoint)  //401 Error Handler
                .accessDeniedHandler(accessDeniedHandler)  //403 Error Handler
                .and()
                .addFilterBefore(new JwtAuthenticationFilter(jwtTokenProvider), UsernamePasswordAuthenticationFilter.class);
    }


    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        // - (3)
        configuration.addAllowedOrigin("*");
        configuration.addAllowedMethod("*");
        configuration.addAllowedHeader("*");
        configuration.setAllowCredentials(true);
        configuration.setMaxAge(3600L);
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
}
