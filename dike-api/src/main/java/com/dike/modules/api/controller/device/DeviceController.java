package com.dike.modules.api.controller.device;


import com.dike.modules.domain.auth.user.CustomUserDetails;
import com.dike.modules.domain.common.response.ApiStatusCode;
import com.dike.modules.domain.common.response.ResponseJsonObject;
import com.dike.modules.domain.device.DeviceService;
import com.dike.modules.domain.device.common.dto.RegisterDeviceDto;
import com.dike.modules.domain.device.common.dto.ReviewKeywordDto;
import com.dike.modules.domain.device.common.dto.SearchDeviceResponseDto;
import com.dike.modules.domain.device.common.dto.SearchKeywordResultDto;
import com.dike.modules.domain.device.common.dto.compare.CompareDeviceListDto;
import com.dike.modules.domain.enums.device.BrandCode;
import com.dike.modules.domain.enums.device.DeviceCategoryCode;
import com.dike.modules.domain.exception.DikeServiceException;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.util.Strings;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Log4j2
@RestController
@RequestMapping(value = "/device")
@RequiredArgsConstructor
public class DeviceController {

    private final DeviceService deviceService;

    @PostMapping("/user")
    public ResponseEntity<ResponseJsonObject> postDevice(@RequestBody @Valid RegisterDeviceDto dto) {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            CustomUserDetails userDetails = (CustomUserDetails) authentication.getPrincipal();

            deviceService.setUserDevice(userDetails.getLoginId(), dto.getBrandCode(),
                    dto.getDeviceType(), dto.getSeries(), dto.getReviewKeywords(), dto.getBuyYear(),
                    dto.getGrade(), dto.getReview());

            ResponseJsonObject response = ResponseJsonObject.builder().code(ApiStatusCode.OK).build();
            return ResponseEntity.status(HttpStatus.OK).body(response);

        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }
    }

    @GetMapping("/{deviceCategoryCode}/{brandCode}/{targetName}")
    public ResponseEntity<ResponseJsonObject> findByDevice(
            @PathVariable("deviceCategoryCode") DeviceCategoryCode deviceCategoryCode,
            @PathVariable("brandCode") BrandCode brandCode,
            @PathVariable("targetName") String targetName) {
        ResponseJsonObject response;

        try {

            List<SearchDeviceResponseDto> result = deviceService.findAllByModelName(deviceCategoryCode,
                    brandCode, targetName);

            if (CollectionUtils.isEmpty(result)) {
                return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
            } else {
                response = ResponseJsonObject.builder().code(ApiStatusCode.OK).data(result).build();
                return ResponseEntity.status(HttpStatus.OK).body(response);
            }
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @GetMapping("/{deviceCategoryCode}/search/series")
    public ResponseEntity<ResponseJsonObject> searchDeviceModelSeries(
            @PathVariable("deviceCategoryCode") DeviceCategoryCode deviceCategoryCode,
            @RequestParam("brand") String brand,
            @RequestParam("searchWord") String searchWord) {

        if (Strings.isBlank(brand)) {
            throw new DikeServiceException(ApiStatusCode.INVALID_PARAMETER, "브랜드는 필수 입력 파라미터입니다.");
        }
        BrandCode brandCode = BrandCode.of(brand);

        if (Strings.isBlank(searchWord)) {
            throw new DikeServiceException(ApiStatusCode.INVALID_PARAMETER, "검색어는 필수 입력 파라미터입니다.");
        }

        try {
            // TODO: empty-string 이 들어왔을때 해당 카테고리의 해당 브랜드에 대한 전체 시리즈명 내려주기
            SearchKeywordResultDto result = deviceService.searchDeviceModelSeriesBy(deviceCategoryCode,
                    brandCode, searchWord);

            if (result.isEmptyKeywords()) {
                return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
            } else {
                return ResponseEntity.status(HttpStatus.OK)
                        .body(ResponseJsonObject.builder().code(ApiStatusCode.OK).data(result).build());
            }
        } catch (RuntimeException e) {
            throw e;
        }
    }


    @GetMapping("/keywords/{deviceCategoryCode}")
    public ResponseEntity<ResponseJsonObject> findByDeviceKeywords(
            @PathVariable("deviceCategoryCode") DeviceCategoryCode deviceCategoryCode) {
        ResponseJsonObject response;

        try {

            List<ReviewKeywordDto> result = deviceService.findAllByDeviceCategoryKeywords(
                    deviceCategoryCode);

            if (CollectionUtils.isEmpty(result)) {
                return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
            } else {
                response = ResponseJsonObject.builder().code(ApiStatusCode.OK).data(result).build();
                return ResponseEntity.status(HttpStatus.OK).body(response);
            }
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @GetMapping("/{deviceCategoryCode}/compare-attributes")
    public ResponseEntity<ResponseJsonObject> getCompareAttributes(
            @PathVariable("deviceCategoryCode") DeviceCategoryCode deviceCategoryCode,
            @RequestParam("brand") String brand,
            @RequestParam("modelSeriesName") String modelSeriesName) {

        if (Strings.isBlank(brand)) {
            throw new DikeServiceException(ApiStatusCode.INVALID_PARAMETER, "브랜드는 필수 입력 파라미터입니다.");
        }
        BrandCode brandCode = BrandCode.of(brand);

        if (Strings.isBlank(modelSeriesName)) {
            throw new DikeServiceException(ApiStatusCode.INVALID_PARAMETER, "모델 시리즈명은 필수 입력 파라미터입니다.");
        }

        return ResponseEntity.status(HttpStatus.OK).body(
                ResponseJsonObject.builder().code(ApiStatusCode.OK).data(
                        deviceService.getDeviceCompareAttributes(deviceCategoryCode, brandCode,
                                modelSeriesName)).build());
    }

    @PostMapping("/{deviceCategoryCode}/compare")
    public ResponseEntity<ResponseJsonObject> compare(@PathVariable DeviceCategoryCode deviceCategoryCode,
                                                      @RequestBody @Valid CompareDeviceListDto compareDevices) {

        return ResponseEntity.status(HttpStatus.OK).body(
                ResponseJsonObject.builder()
                        .code(ApiStatusCode.OK)
                        .data(deviceService.compareDevices(deviceCategoryCode, compareDevices))
                        .build());
    }
}
