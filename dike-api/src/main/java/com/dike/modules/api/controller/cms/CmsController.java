package com.dike.modules.api.controller.cms;

import com.dike.modules.domain.cms.BrandDevice.BrandResponse;
import com.dike.modules.domain.cms.CmsService;
import com.dike.modules.domain.cms.BrandDevice.DeviceCategory;
import com.dike.modules.domain.cms.BrandDevice.DeviceCategoryResponse;
import com.dike.modules.domain.cms.policy.Policy;
import com.dike.modules.domain.cms.policy.PolicyResponse;
import com.dike.modules.domain.cms.youtube.YouTubeApiService;
import com.dike.modules.domain.cms.youtube.dto.YouTubeDto;
import com.dike.modules.domain.cms.youtube.dto.YoutubeRecommendResponse;
import com.dike.modules.domain.common.response.ApiStatusCode;
import com.dike.modules.domain.common.response.ResponseJsonObject;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Class       : CmsController
 * Author      : 조 준 희
 * Description : Class Description
 * History     : [2022-07-22] - 조 준희 - Class Create
 */
@Log4j2
@RestController
@RequestMapping(value = "/cms")
public class CmsController {

    private CmsService cmsService;
    private YouTubeApiService youTubeApiService;

    @Autowired
    public CmsController(CmsService cmsService, YouTubeApiService youTubeApiService) {

        this.cmsService = cmsService;
        this.youTubeApiService = youTubeApiService;
    }

    /**
     * Description : device 카테고리 조회.
     * Author      : 조 준 희
     * History     : [2023-06-01] - 조 준 희 - Create
     */
    @GetMapping("/devices")
    public ResponseEntity<ResponseJsonObject> devices() {
        try {
           // authService.join(dto);

            List<DeviceCategory> deviceCategories = cmsService.findAllDeviceCategories();

            List<DeviceCategoryResponse> deviceCategoryResponses = deviceCategories.stream()
                    .map(e -> e.toResponse(e) ).collect(Collectors.toList());


            if(deviceCategoryResponses == null || deviceCategoryResponses.size() == 0)
            {
                return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
            }

            ResponseJsonObject response = ResponseJsonObject.builder().code(ApiStatusCode.OK).data(deviceCategoryResponses).build();
            return ResponseEntity.status(HttpStatus.OK).body(response);

        }catch(RuntimeException e){
            throw e;
        }
    }
    
    /**
     * Description : device 카테고리 매핑된 브랜드 조회
     * Author      : 조 준 희
     * History     : [2023-06-01] - 조 준 희 - Create
     */
    @GetMapping("/{deviceCategoryId}/brand")
    public ResponseEntity<ResponseJsonObject> brands(@PathVariable(name = "deviceCategoryId") Long deviceCategoryId) {
        try {
            // authService.join(dto);

            List<BrandResponse> brands = cmsService.findAllBrandofDeviceCategory(deviceCategoryId);

            if(brands == null || brands.size() == 0)
            {
                return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
            }

            ResponseJsonObject response = ResponseJsonObject.builder().code(ApiStatusCode.OK).data(brands).build();
            return ResponseEntity.status(HttpStatus.OK).body(response);

        }catch(RuntimeException e){
            throw e;
        }
    }

    /**
     * Description : 이용약관 조회
     * Author      : 조 준 희
     * History     : [2023-06-01] - 조 준 희 - Create
     */
    @GetMapping("/policy")
    public ResponseEntity<ResponseJsonObject> policy() {
        try {

            // 설정된 모든 이용약관 조회하기.
            List<Policy> policy = cmsService.findAllPolicy();

            // 응답 값 설정.
            List<PolicyResponse> policyResponses = policy.stream()
                    .map(e -> e.toResponse(e) ).collect(Collectors.toList());

            // 설정된 이용약관이 1건도 없는 경우. 204 Non Content Response
            if(policyResponses == null || policyResponses.size() == 0)
            {
                return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
            }

            // 응답.
            ResponseJsonObject response = ResponseJsonObject.builder().code(ApiStatusCode.OK).data(policyResponses).build();
            return ResponseEntity.status(HttpStatus.OK).body(response);

        }catch(RuntimeException e){
            throw e;
        }
    }


    /**
     * Description : 유튜브 추천 영상 조회.
     * Author      : 조 준 희
     * History     : [2023-06-01] - 조 준 희 - Create
     */
    @GetMapping("/youtube/recommended")
    public ResponseEntity<ResponseJsonObject> yotubeVideos() {
        try {

            List<YoutubeRecommendResponse> videos = youTubeApiService.YoutubeVideos();

            if(videos == null){
                ResponseJsonObject response = ResponseJsonObject.builder().code(ApiStatusCode.NO_CONTENT).build();
                return ResponseEntity.status(HttpStatus.NO_CONTENT.ordinal()).body(response);
            }

            ResponseJsonObject response = ResponseJsonObject.builder().code(ApiStatusCode.OK).data(videos).build();
            return ResponseEntity.status(HttpStatus.OK).body(response);

        }catch(RuntimeException e){
            throw e;
        }
    }
}
