package com.dike.modules.api.controller.user;


import com.dike.modules.domain.auth.AuthService;
import com.dike.modules.domain.auth.dto.*;
import com.dike.modules.domain.auth.user.CustomUserDetails;
import com.dike.modules.domain.common.auth.JwtAuthenticationFilter;
import com.dike.modules.domain.common.response.ApiStatusCode;
import com.dike.modules.domain.common.response.ResponseJsonObject;
import com.dike.modules.domain.common.sms.MessageSendService;
import com.dike.modules.domain.exception.DikeServiceException;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import static com.dike.modules.domain.common.constants.DikeConstant.PASSWORD_RECOVER_TOKEN;

@Log4j2
@RestController
@RequestMapping(value = "/user")
@RequiredArgsConstructor
public class DikeUserController {

    private final AuthService authService;
    private final MessageSendService messageSendService;


    /**
     * Description : 사용자 회원가입.
     * Author      : 조 준 희
     * History     : [2023-06-01] - 조 준 희 - Create
     */
    @PostMapping("/join")
    public ResponseEntity<ResponseJsonObject> join(@RequestBody @Valid UserJoinDto dto) {
        try {
            authService.join(dto);

            ResponseJsonObject response = ResponseJsonObject.builder().code(ApiStatusCode.OK).build();
            return ResponseEntity.status(HttpStatus.OK).body(response);

        } catch (RuntimeException e) {
            throw e;
        }
    }

    /**
     * Description : login ID의 정보 중복 체크. ( 회원가입 시 아이디 중복체크에 사용된다. )
     * Author      : 조 준 희
     * History     : [2023-06-01] - 조 준 희 - Create
     */
    @GetMapping("/check/duplication/{loginId}")
    public ResponseEntity<ResponseJsonObject> checkIdDuplicated(
            @PathVariable @NotBlank String loginId) {
        try {

            LoginIdCheckDto result = authService.checkDuplicationId(loginId);

            ResponseJsonObject response = ResponseJsonObject.builder().code(ApiStatusCode.OK).data(result)
                    .build();
            return ResponseEntity.status(HttpStatus.OK).body(response);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    /**
     * Description : SMS 인증문자 발송  ( 회원가입, 패스워드 변경 등 여러 기능에서 SMS 인증번호를 발송하는 API )
     * Author      : 조 준 희
     * History     : [2023-06-01] - 조 준 희 - Create
     */
    @GetMapping("/send/certification")
    public ResponseEntity<ResponseJsonObject> sendMessage(@RequestParam String receiver,
                                                          @RequestParam(required = false) String loginId,
                                                          @RequestParam(required = false, defaultValue = "false") Boolean forPasswordRecover) {

        try {
            // 패스워드 복구가 아닌 경우.
            if (!forPasswordRecover && authService.existsPhoneNum(receiver)) {
                ResponseJsonObject response = ResponseJsonObject.builder()
                        .code(ApiStatusCode.ALREADY_REGISTERED_USER).build();
                return ResponseEntity.status(HttpStatus.OK).body(response);
            }

            // 패드워드 복구 SMS 인증번호 발송인 경우.
            if (forPasswordRecover && !authService.checkValidLoginIdWithPhoneNumber(loginId, receiver)) {
                throw new DikeServiceException(ApiStatusCode.UNAUTHORIZED, "해당 휴대폰으로 가입된 아이디가 존재하지 않습니다.");
            }

            // SMS 발송.
            messageSendService.sendMessageBySens(receiver);

            // 응답.
            ResponseJsonObject response = ResponseJsonObject.builder().code(ApiStatusCode.OK).build();
            return ResponseEntity.status(HttpStatus.OK).body(response);
        }
        catch(DikeServiceException e){
            throw e;
        }
    }

    /**
     * Description : 사용자 로그인
     * Author      : 조 준 희
     * History     : [2023-06-01] - 조 준 희 - Create
     */
    @PostMapping("/login")
    public ResponseEntity<ResponseJsonObject> login(@RequestBody @Valid UserLoginDto dto)
            throws DikeServiceException {
        try {
            // 로그인
            UserTokenDto result = authService.login(dto);

            // 생성된 액세스 토큰, 리프레시 토큰 헤더 전달.
            HttpHeaders headers = new HttpHeaders();
            headers.set(JwtAuthenticationFilter.ACCESS_TOKEN, result.getAccessToken());
            headers.set(JwtAuthenticationFilter.REFRESH_TOKEN, result.getRefreshToken());

            // 응답.
            ResponseJsonObject response = ResponseJsonObject.builder().code(ApiStatusCode.OK).build();
            return ResponseEntity.status(HttpStatus.OK).headers(headers).body(response);

        } catch (DikeServiceException e) {
            throw e;
        }
    }

    /**
     * Description : 사용자 로그아웃.
     * Author      : 조 준 희
     * History     : [2023-06-01] - 조 준 희 - Create
     */
    @PostMapping("/logout")
    public ResponseEntity<ResponseJsonObject> logout(
            @RequestHeader(JwtAuthenticationFilter.ACCESS_TOKEN) String accessToken,
            @RequestHeader(JwtAuthenticationFilter.REFRESH_TOKEN) String refreshToken) {
        try {

            // JWT 필터에서 저장된 사용자 인가 정보를 가져온다.
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            // 사용자 상세 정보 변환
            CustomUserDetails userDetails = (CustomUserDetails) authentication.getPrincipal();
            String loginId = userDetails.getLoginId();

            // 로그아웃 처리.
            authService.logout(UserTokenDto.of(accessToken, refreshToken), loginId);

            ResponseJsonObject response = ResponseJsonObject.builder().code(ApiStatusCode.OK).build();
            return ResponseEntity.status(HttpStatus.OK).body(response);

        } catch (RuntimeException e) {
            throw e;
        }
    }

    @PostMapping("/check/certification")
    public ResponseEntity<ResponseJsonObject> certification(@RequestBody CertificationDto dto)
            throws DikeServiceException {
        try {
            messageSendService.checkCertification(dto);

            ResponseJsonObject response = ResponseJsonObject.builder().code(ApiStatusCode.OK).build();
            return ResponseEntity.status(HttpStatus.OK).body(response);

        } catch (DikeServiceException e) {
            throw e;
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @PostMapping("/reissue")
    public ResponseEntity<ResponseJsonObject> reissue(
            @RequestHeader(JwtAuthenticationFilter.REFRESH_TOKEN) String refreshToken)
            throws DikeServiceException {
        try {

            UserTokenDto result = authService.reissue(refreshToken);
            ResponseJsonObject response = ResponseJsonObject.builder().code(ApiStatusCode.OK).data(result)
                    .build();

            return ResponseEntity.status(HttpStatus.OK).body(response);

        } catch (DikeServiceException e) {
            throw e;
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @DeleteMapping("/delete")
    public ResponseEntity<ResponseJsonObject> delete() throws DikeServiceException {
        try {

            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            CustomUserDetails userDetails = (CustomUserDetails) authentication.getPrincipal();

            authService.delete(userDetails.getLoginId());
            ResponseJsonObject response = ResponseJsonObject.builder().code(ApiStatusCode.OK).build();

            return ResponseEntity.status(HttpStatus.OK).body(response);

        } catch (DikeServiceException e) {
            throw e;
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @PostMapping("/check/password-certification")
    public ResponseEntity<ResponseJsonObject> sendMessageWithToken(
            @RequestBody @Valid PasswordCertificationDto dto) {
        messageSendService.checkCertification(dto);
        if (!authService.checkValidLoginIdWithPhoneNumber(dto.getLoginId(), dto.getReceiver())) {
            throw new DikeServiceException(ApiStatusCode.UNAUTHORIZED,
                    "해당 휴대폰으로 가입된 아이디가 일치하지 않아 비밀번호 변경이 불가합니다.");
        }
        String passwordRecoverToken = authService.createPasswordRecoverToken(dto.getLoginId());
        HttpHeaders headers = new HttpHeaders();
        headers.set(PASSWORD_RECOVER_TOKEN, passwordRecoverToken);
        ResponseJsonObject response = ResponseJsonObject.builder().code(ApiStatusCode.OK).build();

        return ResponseEntity.status(HttpStatus.OK).headers(headers).body(response);
    }

    @PutMapping("/password/recover")
    public ResponseEntity<ResponseJsonObject> recoverPassword(
            @RequestHeader(PASSWORD_RECOVER_TOKEN) String passwordRecoverToken,
            @RequestBody @Valid PasswordRecoverDto dto) {
        authService.recoverPassword(passwordRecoverToken, dto);
        return ResponseEntity.ok(ResponseJsonObject.builder().code(ApiStatusCode.OK).build());
    }
}
