package com.dike.modules.api;


import com.dike.modules.domain.configuration.database.DikePostgreSqlConfig;
import com.dike.modules.domain.configuration.redis.RedisConfig;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;

@SpringBootApplication
@ComponentScan(basePackages = "com.dike.modules")
@EnableRedisRepositories(basePackages = "com.dike.modules")
@Import({DikePostgreSqlConfig.class, RedisConfig.class})
@Log4j2
public class DikeApiApplication {

    public static void main(String[] args) {
        // log4j2 ThreadContext Map Child Thread Inheritable Setting
        System.setProperty("isThreadContextMapInheritable", "true");
        SpringApplication.run(DikeApiApplication.class, args);
        log.info("Dike Application Log Level : {} ", log.getLevel());

    }
}
