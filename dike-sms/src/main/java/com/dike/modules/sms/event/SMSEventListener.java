package com.dike.modules.sms.event;

import com.dike.modules.sms.component.SMSSender;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Class       : SMSEventListener
 * Author      : 조 준 희
 * Description : SMS Event 리스너.
 * History     : [2022-07-04] - 조 준희 - Class Create
 */
@Component
@EnableAsync
@Log4j2
public class SMSEventListener {
    private final SMSSender smsSender;

    @Autowired
    public SMSEventListener(SMSSender smsSender) {
        this.smsSender = smsSender;
    }

    @Async("smsAsync")
    @EventListener
    public void onApplicationEvent(SMSEvent event){
        try {
            smsSender.sendAuthMessage(event.getPhoneNum(), event.getSmsAuthNum());

        } catch (IOException e) {
            log.error("SMS Send Exception : {}", e.toString());
            return ;
        } catch (InterruptedException e) {
            log.error("SMS Send Exception : {}", e.toString());
            return ;
        }

        log.debug("SMS Send 성공.");
    }
}
