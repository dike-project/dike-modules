## Image 서버 Dockerfile

FROM openjdk:11

LABEL service="dike-api" jdk-ver="11"

RUN mkdir -p /dike
#RUN apk update
## openjdk 8 tz 설정.
RUN groupadd -g 999 dike
RUN useradd -r -u 999 -g dike dike

ARG JAR_FILE=dike-api/build/libs/*.jar

## jenkins workspace jar 파일 이동.
ADD ${JAR_FILE} /dike/dike-api.jar

## CMD, ENTRYPOINT 실행 경로.
WORKDIR /dike

## profiles 설정 버전
## ENTRYPOINT ["javar", "-jar", "-Dspring.profiles.active=dev", "dike-api-0.0.1-SNAPSHOT.jar"]
ENTRYPOINT ["java","-jar","-Dspring.profiles.active=dev","dike-api.jar"]
